	- trebuie adaugat fisierul libs din framework
	- INAINTE DE A RULA: in Tema3.h este o variabila settingsL la linia 125.
In functie de valoarea sa se randeaza mai multe sau mai putine lumini. 1 - 16
lumini, 2 - 80 de lumini, 3 - 144 de lumini, in functie de zona in care se afla
observatorul. Este setata pe 1.
	Structura fisierului Source/Teme/Tema3:
	
	/Shaders 
	|	+----FragmentShaderColor.glsl
	|	+----FragmentShaderTexture.glsl
	|	+----VertexShaderColor.glsl
	|	+----VertexShaderTexture.glsl
	/Textures
	|	+----apartments1.png
	|	+----asphalt.png
	|	+----concrete.png
	|	+----concrete2.png
	|	+----grass.png
	|	+----modern-building1.png
	|	+----modern-building2.png
	|	+----old-building.png
	|	+----pavement.png
	|	+----sand.png
	|	+----skybox.png
	+----Multiples.cpp
	+----Multiples.h
	+----Street.cpp
	+----Street.h
	+----Tema3.cpp
	+----Tema3.h

	Se folosesc doua shadere, unul pentru elemente texturate si unul pentru
elemente ce folosesc culori solide, precum apa si lampile stradale; ambele
implementeaza iluminarea cu mai multe lumini.
	In Multiples.h sunt definite structuri pentru generarea mapei, proprietati
de materiale si de lumina si clasa Complex, ce este folosita pentru crearea
meshelor pentru apa, parcuri si asfalt
	In Street.h este definita clasa Streets, iar in Street.cpp sunt completate
metodele din aceasta.
	Ideea pe care am aplicat-o la majoritatea meshelor este urmatoarea: tot ce
foloseste aceeasi textura este pus intr-un singur mesh pentru a face totul cat
mai smooth(facand exceptie de lumini). De asemenea, indicele uneori trecea de
maximul pentru unsigned short, asa ca in loc sa fac, de exemplu, un singur mesh
pentru strazi, am facut un vector de mesh-uri care contine mesh-urile pentru
strazi;
	Ordinea metodelor apelate si ce face fiecare:
	GeneratePlane() - initializeaza suprafata de lucru de 8km pe 8km si apeleaza
urmatoarele 5 metode:
	AddHighway() - adauga o sosea principala cu 4 benzi care are o directie
generala si se poate abate de la aceasta, dar in principiu merge in directia
generala si nu se intoarce in directie din care a plecat. Are o sansa sa se
bifurce, iar punctul de inceput al acesteia este ales la intamplare. Se
pastreaza cateva puncte importante din care sunt generate strazile secundare cu
2 benzi
    AddRoads() - adauga, incepand din punctele importante de mai devreme strazi,
astfel incat sa nu depaseasca un numar maxim de strazi; este o functie
recursiva, asemanatoare cu flood fill, doar ca are sanse sa nu mearga in anumite
directii;
	CreateLand() - care primeste o zona descoperita, fara strazi, pe care o
populeaza cu pamant, apa, parcuri sau asfalt simplu. Am vrut sa adaug pe
bucatile de asfalt niste zona mai importante, cum ar fi un carnaval sau un
spital sau ceva asemanator, doar ca nici nu am gasit ceva cat de cat
interesant si dintr-un anumit motiv, cand am incercat sa mai adaug copaci in
parcuri se bloca la pornire;
	CreateStreetMeshes() - analizeaza fiecare strada, decide cate puncte de
acces are o intersectie si apeleaza metodele din clasa Streets;
	CreateComplexMeshes() - creeaza mesh-urile pentru parcuri, apa si asfalt in
aceeasi idee ca la strazi: un singur mesh pentru apa, un singur mesh pentru
parcuri si un singur mesh pentru asfalt, cu toate ca au forme neregulate;
	CreateBuildingsSideWalks() - l-am pus cu 0.05 unitati mai jos decat restul
mesh-urilor orizontale; ideea e ca verifica ce tip de strada exista si unde e
acea strada, cate iesire are si creeaza mesh-ul pentru padele in jurul strazii;
e lunga pentru ca sunt foarte multe cazuri si nu aveam ceva mai eficient din
punctul de vedere al lungimii codului la momentul respectiv in minte;
	AddLampPosts() - adauga felinare la fiecare colt de strada sau langa sosele
si apeleaza urmatoarele 2 metode:
	AddLampPost() - adauga in vectorii pentru creat mesh-ul / mesh-urile pentru
felinare un felinar; in momentul asta, felinarele nu sunt cilindrice, ci au 4
fete; se poate modifica din variabila coef de la inceputul metodei; apeleaza
urmatoarea metoda:
	AddSpotlight() - care adauga o lumina de tip spotlight intr-o pozitie(e
apelata doar din AddLampPost(), deci in varful unui felinar), o pune intr-un
vector si indicele corespunzator acesteia in vectorul respectiv este apoi pus in
functie de variabila settingsL in anumite pozitii pentru a se aprinde doar daca
observatorul este in chunk-ul respectiv/intr-unul din chunkurile apropiate de
aceasta; am facut asta ca sa nu calculez de fiecare data distanta de la
observator la lumini;
	endLampPost() - initializeaza ultimul mesh ramas in vectorul de mesh-uri
pentru felinare;
	CreateBuildingSquares() - care verifica daca se pot pune cladiri intr-o
anumita zona; spawneaza ori o cladire, ori doua, ori trei, ori patru intr-o zona
delimitata de strazi; apeleaza urmatoarele 6 metode:
	CreateTriangleBuilding() - cladire relativ hardcodata, doua cladiri in forma
de prisme cu sectiunea triunghiuri dreptunghice isoscele cu ipotenuzele
paralele, fiecare avand inaltimea la intamplare;
	CreateCilinderBuilding() - tot hardcodata, din doua bucati semicilindri, de
inaltimi diferite;
	CreateLApartments() - tot hardcodata, cladire in forma de L, inaltimea
aleasa la intamplare;
	CreateUApartments() - tot hardcodata, cladire in forma de U, inaltimea
aleasa la intamplare;
	CreateRandomBuilding() - majoritate cladirilor de pe harta sunt random;
primeste o zona in care sa creeze o cladire, isi alege un numar de forme pe care
sa le includa in cladire, si pentru fiecare forma alege un centru, numarul de
fete si dimensiunile acesteia astfel incat sa nu se suprapuna cu strada sau cu
alte cladiri; unele cladiri au sansa sa aiba un km inaltime;
	endBuildingSquares() - daca mai raman mesh-uri neinitializate, le
initializeaza;
	PrintASCIIMap() - printeaza in terminal harta orasului sub forma de
caractere ASCII;