#include "Tema3.h"


void Tema3::PrintASCIIMap() {
    std::cout << "+";
    for (int i = 0; i < CITYS; i++) {
        std::cout << "-";
    }
    std::cout << "+\n";
    for (int i = 0; i < CITYS; i++) {
        std::cout << "|";
        for (int j = 0; j < CITYS; j++) {
            bool left = false, right = false, up = false, down = false;
            int dirs = 0;
            char toPrint = 146;
            if (matrix[i][j] == ROAD) {
                if (i != 0) {
                    if (matrix[i - 1][j] == ROAD
                        || matrix[i - 1][j] == HIGHWAY) {
                        up = true;
                        dirs++;
                    }
                }
                if (j != 0) {
                    if (matrix[i][j - 1] == ROAD
                        || matrix[i][j - 1] == HIGHWAY) {
                        left = true;
                        dirs++;
                    }
                }
                if (i != CITYS - 1) {
                    if (matrix[i + 1][j] == ROAD
                        || matrix[i + 1][j] == HIGHWAY) {
                        down = true;
                        dirs++;
                    }
                }
                if (j != CITYS - 1) {
                    if (matrix[i][j + 1] == ROAD
                        || matrix[i][j + 1] == HIGHWAY) {
                        right = true;
                        dirs++;
                    }
                }
                if (dirs == 4) {
                    toPrint = 206;
                } else if (dirs == 3) {
                    if (left && right & up) {
                        toPrint = 202;
                    } else if (left && right && down) {
                        toPrint = 203;
                    } else if (left && down && up) {
                        toPrint = 185;
                    } else if (right && down && up) {
                        toPrint = 204;
                    }
                } else if (dirs == 1) {
                    if (left) {
                        toPrint = '>';
                    } else if (right) {
                        toPrint = '<';
                    } else if (up) {
                        toPrint = 'U';
                    } else {
                        toPrint = 'A';
                    }
                } else if (left && right) {
                    toPrint = 205;
                } else if (up && down) {
                    toPrint = 186;
                } else if (up && left) {
                    toPrint = 188;
                } else if (up && right) {
                    toPrint = 200;
                } else if (down && left) {
                    toPrint = 187;
                } else if (down && right) {
                    toPrint = 201;
                }
            } else if (matrix[i][j] == HIGHWAY) {
                toPrint = 254;
            } else if (matrix[i][j] == LAND) {
                toPrint = 176;
            } else if (matrix[i][j] == WATER) {
                toPrint = ' ';
            } else if (matrix[i][j] == PARK) {
                toPrint = '~';
            } else if (matrix[i][j] == BIGB) {
                toPrint = 233;
            } else if (matrix[i][j] == NOTHING) {
                toPrint = 'X';
            }
            std::cout << toPrint;
        }
        std::cout << "|" << std::endl;
    }
    std::cout << "+";
    for (int i = 0; i < CITYS; i++) {
        std::cout << "-";
    }
    std::cout << "+\n";
}

void Tema3::AddRoads(int line, int col) {
    if (capacityCS == 0 || matrix[line][col] == ROAD || matrix[line][col] == HIGHWAY) {
        return;
    }
    matrix[line][col] = ROAD;
    capacityCS--;
    int chance = rand() % 2;
    if (line != CITYS - 1 && chance == 1) {
        AddRoads(line + 1, col);
    }
    chance = rand() % 2;
    if (col != CITYS - 1 && chance == 1) {
        AddRoads(line, col + 1);
    }
    chance = rand() % 2;
    if (line != 0 && chance == 1) {
        AddRoads(line - 1, col);
    }
    chance = rand() % 2;
    if (col != 0 && chance == 1) {
        AddRoads(line, col - 1);
    }
}

void Tema3::AddHighway(int line, int col, direction genDirection, direction currDirection) {
    if ((line == CITYS && currDirection == DOWN)
        || (col == CITYS && currDirection == RIGHT)
        || (line == -1 && currDirection == UP)
        || (col == -1 && currDirection == LEFT)) {
        return;
    } else if (line >= CITYS || col >= CITYS
        || line < 0 || col < 0) {
        return;
    } else if (matrix[line][col] == 2) {
        return;
    }
    matrix[line][col] = HIGHWAY;
    capacityCS--;
    if (((currDirection == RIGHT || currDirection == LEFT) && (col == CITYS / 3 || col == 2 * CITYS / 3))
        || ((currDirection == DOWN || currDirection == UP) && (line == CITYS / 3 || line == 2 * CITYS / 3))) {
        roadGenerator toAdd;
        toAdd.line = line;
        toAdd.column = col;
        divergents.push_back(toAdd);
    }

    float div = (float)(rand() % 100) + (float)(rand() % 1000) / 1000;
    if (div > highwayDivThreshold) {
        int chance = rand() % 2;
        if (chance == 0) {
            if (currDirection == LEFT
                || currDirection == RIGHT) {
                AddHighway(line + 1, col, DOWN, DOWN);
                if (currDirection == LEFT) {
                    AddHighway(line, col - 1, genDirection, LEFT);
                } else {
                    AddHighway(line, col + 1, genDirection, RIGHT);
                }
            } else {
                AddHighway(line, col + 1, RIGHT, RIGHT);
                if (currDirection == UP) {
                    AddHighway(line - 1, col, genDirection, UP);
                } else {
                    AddHighway(line + 1, col, genDirection, DOWN);
                }
            }
        } else {
            if (currDirection == LEFT
                || currDirection == RIGHT) {
                AddHighway(line - 1, col, UP, UP);
                if (currDirection == LEFT) {
                    AddHighway(line, col - 1, genDirection, LEFT);
                } else {
                    AddHighway(line, col + 1, genDirection, RIGHT);
                }
            } else {
                AddHighway(line, col - 1, LEFT, LEFT);
                if (currDirection == UP) {
                    AddHighway(line - 1, col, genDirection, UP);
                } else {
                    AddHighway(line + 1, col, genDirection, DOWN);
                }
            }
        }
    } else {
        int chance = rand() % 100;
        if (genDirection == DOWN
            || genDirection == UP) {
            if (currDirection == genDirection && chance > 75) {
                int dir = rand() % 2;
                if (dir == 0) {
                    AddHighway(line, col - 1, genDirection, LEFT);
                } else {
                    AddHighway(line, col + 1, genDirection, RIGHT);
                }
            } else if (genDirection == UP) {
                AddHighway(line - 1, col, genDirection, UP);
            } else if (genDirection == DOWN) {
                AddHighway(line + 1, col, genDirection, DOWN);
            }
        } else {
            if (currDirection == genDirection && chance > 75) {
                int dir = rand() % 2;
                if (dir == 0) {
                    AddHighway(line - 1, col, genDirection, UP);
                } else {
                    AddHighway(line + 1, col, genDirection, DOWN);
                }
            } else if (genDirection == LEFT) {
                AddHighway(line, col - 1, genDirection, LEFT);
            } else if (genDirection == RIGHT) {
                AddHighway(line, col + 1, genDirection, RIGHT);
            }
        }
    }
}

void Tema3::GeneratePlane() {
    for (int i = 0; i < CITYS; i++) {
        for (int j = 0; j < CITYS; j++) {
            matrix[i][j] = UNDEFINED;
        }
    }

    int sideStart = rand() % 2;
    if (sideStart == 0) {
        AddHighway(0, rand() % (CITYS / 4) + CITYS / 2, DOWN, DOWN);
    } else {
        AddHighway(rand() % (CITYS / 4) + CITYS / 2, 0, RIGHT, RIGHT);
    }
    PrintASCIIMap();
    for (auto& it : divergents) {
        if (capacityCS == 0) {
            break;
        }
        if (matrix[it.line][it.column - 1] == UNDEFINED) {
            AddRoads(it.line, it.column - 1);
        }
        if (matrix[it.line][it.column + 1] == UNDEFINED) {
            AddRoads(it.line, it.column + 1);
        }
        if (matrix[it.line - 1][it.column] == UNDEFINED) {
            AddRoads(it.line - 1, it.column);
        }
        if (matrix[it.line + 1][it.column] == UNDEFINED) {
            AddRoads(it.line + 1, it.column);
        }
    }
    if (capacityCS > maxCS / 2) {
        int i, j;
        bool done = false;
        for (i = 0; i < CITYS && !done; i++) {
            for (j = 0; j < CITYS; j++) {
                if ((matrix[i][j] == ROAD || matrix[i][j] == HIGHWAY) && j != 0) {
                    done = true;
                    break;
                }
            }
        }
        AddRoads(i, j - 1);
    }
    if (capacityCS > maxCS / 2) {
        int i, j;
        bool done = false;
        for (i = 0; i < CITYS && !done; i++) {
            for (j = CITYS - 1; j >= 0; j--) {
                if ((matrix[i][j] == ROAD || matrix[i][j] == HIGHWAY) && j != CITYS - 1) {
                    done = true;
                    break;
                }
            }
        }
        AddRoads(i, j + 1);
    }
    if (capacityCS > maxCS / 2) {
        int i, j;
        bool done = false;
        for (i = CITYS - 1; i >= 0 && !done; i--) {
            for (j = 0; j < CITYS; j++) {
                if ((matrix[i][j] == ROAD || matrix[i][j] == HIGHWAY) && i != CITYS - 1) {
                    done = true;
                    break;
                }
            }
        }
        AddRoads(i + 1, j);
    }
    if (capacityCS > maxCS / 2) {
        int i, j;
        bool done = false;
        for (i = CITYS - 1; i >= 0 && !done; i--) {
            for (j = CITYS - 1; j >= 0; j--) {
                if ((matrix[i][j] == ROAD || matrix[i][j] == HIGHWAY) && i != 0) {
                    done = true;
                    break;
                }
            }
        }
        AddRoads(i - 1, j);
    }
    std::cout << std::endl << std::endl;
    PrintASCIIMap();

    std::list<undiscovered> lands;

    for (int i = 0; i < CITYS; i++) {
        for (int j = 0; j < CITYS; j++) {
            if (matrix[i][j] == UNDEFINED) {
                undiscovered land;
                land.line = i;
                land.column = j;
                land.size = 0;
                land.interior = true;
                Discover(&land, i, j, NOTHING);
                lands.push_back(land);
            }
        }
    }
    lands.sort([](const undiscovered& land1, const undiscovered& land2) {return land1.size > land2.size; });
    int sl = lands.front().line;
    int sc = lands.front().column;
    lands.pop_front();
    CreateLand(sl, sc, WATER);

    while (lands.size() != 0) {
        sl = lands.front().line;
        sc = lands.front().column;
        int x;
        if (lands.front().interior) {
            x = rand() % 4;
            if (x == 1) {
                x = BIGB;
            } else {
                x = PARK;
            }
        } else {
            x = rand() % 4;
            if (x == 1) {
                x = WATER;
            } else {
                x = LAND;
            }
        }
        CreateLand(sl, sc, x);
        lands.pop_front();
    }
    PrintASCIIMap();

    CreateStreetMeshes();
    CreateComplexMeshes();
}

void Tema3::CreateLand(int line, int col, int type) {
    if (line < 0 || line >= CITYS || col < 0 || col >= CITYS
        || matrix[line][col] != -1) {
        return;
    }
    matrix[line][col] = type;
    CreateLand(line - 1, col, type);
    CreateLand(line + 1, col, type);
    CreateLand(line, col - 1, type);
    CreateLand(line, col + 1, type);
}

void Tema3::Discover(undiscovered *land, int line, int col, int type) {
    if (line < 0 || line >= CITYS || col < 0 || col >= CITYS
        || matrix[line][col] != 0) {
        return;
    }
    if (line == 0 || line == CITYS - 1
        || col == 0 || col == CITYS - 1) {
        land->interior = false;
    }
    matrix[line][col] = type;
    land->size++;
    Discover(land, line - 1, col, type);
    Discover(land, line + 1, col, type);
    Discover(land, line, col - 1, type);
    Discover(land, line, col + 1, type);
}

void Tema3::CreateStreetMeshes() {
    float halfLength = streetLength / 2;
    float H = 0.2;
    for (int i = 0; i < CITYS; i++) {
        for (int j = 0; j < CITYS; j++) {
            bool left = false, right = false, up = false, down = false;
            int dirs = 0;
            if (matrix[i][j] == ROAD) {
                if (i != 0) {
                    if (matrix[i - 1][j] == ROAD
                        || matrix[i - 1][j] == HIGHWAY) {
                        up = true;
                        dirs++;
                    }
                }
                if (j != 0) {
                    if (matrix[i][j - 1] == ROAD
                        || matrix[i][j - 1] == HIGHWAY) {
                        left = true;
                        dirs++;
                    }
                }
                if (i != CITYS - 1) {
                    if (matrix[i + 1][j] == ROAD
                        || matrix[i + 1][j] == HIGHWAY) {
                        down = true;
                        dirs++;
                    }
                }
                if (j != CITYS - 1) {
                    if (matrix[i][j + 1] == ROAD
                        || matrix[i][j + 1] == HIGHWAY) {
                        right = true;
                        dirs++;
                    }
                }
                float X = -((float)(i - CITYS / 2)) / 2 - streetLength;
                float Z = ((float)(j - CITYS / 2)) / 2;
                roadMap[i][j].counter = 0;
                if (dirs == 1) {
                    if (left) {
                        strs.CreateSimpleStreet(LEFT, glm::vec3(X, H, Z), streetLength, roadWidth, highwayWidth);
                        roadMap[i][j].left = ROAD;
                        roadMap[i][j].right = NOTHING;
                        roadMap[i][j].up = NOTHING;
                        roadMap[i][j].down = NOTHING;
                        roadMap[i][j].counter = 1;
                    } else if (right) {
                        strs.CreateSimpleStreet(RIGHT, glm::vec3(X, H, Z), streetLength, roadWidth, highwayWidth);
                        roadMap[i][j].left = NOTHING;
                        roadMap[i][j].right = ROAD;
                        roadMap[i][j].up = NOTHING;
                        roadMap[i][j].down = NOTHING;
                        roadMap[i][j].counter = 1;
                    } else if (up) {
                        strs.CreateSimpleStreet(UP, glm::vec3(X, H, Z), streetLength, roadWidth, highwayWidth);
                        roadMap[i][j].left = NOTHING;
                        roadMap[i][j].right = NOTHING;
                        roadMap[i][j].up = ROAD;
                        roadMap[i][j].down = NOTHING;
                        roadMap[i][j].counter = 1;
                    } else if (down) {
                        strs.CreateSimpleStreet(DOWN, glm::vec3(X, H, Z), streetLength, roadWidth, highwayWidth);
                        roadMap[i][j].left = NOTHING;
                        roadMap[i][j].right = NOTHING;
                        roadMap[i][j].up = NOTHING;
                        roadMap[i][j].down = ROAD;
                        roadMap[i][j].counter = 1;
                    }
                } else if (dirs >= 2) {
                    if (left) {
                        strs.CreateMeshStreet(LEFT, glm::vec3(X, H, Z), streetLength, roadWidth, highwayWidth);
                        roadMap[i][j].left = ROAD;
                        roadMap[i][j].counter++;
                    } else {
                        roadMap[i][j].left = NOTHING;
                    }
                    if (right) {
                        strs.CreateMeshStreet(RIGHT, glm::vec3(X, H, Z), streetLength, roadWidth, highwayWidth);
                        roadMap[i][j].right = ROAD;
                        roadMap[i][j].counter++;
                    } else {
                        roadMap[i][j].right = NOTHING;
                    }
                    if (up) {
                        strs.CreateMeshStreet(UP, glm::vec3(X, H, Z), streetLength, roadWidth, highwayWidth);
                        roadMap[i][j].up = ROAD;
                        roadMap[i][j].counter++;
                    } else {
                        roadMap[i][j].up = NOTHING;
                    }
                    if (down) {
                        strs.CreateMeshStreet(DOWN, glm::vec3(X, H, Z), streetLength, roadWidth, highwayWidth);
                        roadMap[i][j].down = ROAD;
                        roadMap[i][j].counter++;
                    } else {
                        roadMap[i][j].down = NOTHING;
                    }
                    strs.CreateMeshStreet(CENTER, glm::vec3(X, H, Z), streetLength, roadWidth, highwayWidth);
                }
            } else if (matrix[i][j] == 2) {
                int adjDirs = 0;
                bool adjUp = false, adjDown = false, adjLeft = false, adjRight = false;
                if (i != 0) {
                    if (matrix[i - 1][j] == HIGHWAY) {
                        up = true;
                        dirs++;
                    } else if (matrix[i - 1][j] == ROAD) {
                        adjUp = true;
                        adjDirs++;
                    }
                }
                if (j != 0) {
                    if (matrix[i][j - 1] == HIGHWAY) {
                        left = true;
                        dirs++;
                    } else if (matrix[i][j - 1] == ROAD) {
                        adjLeft = true;
                        adjDirs++;
                    }
                }
                if (i != CITYS - 1) {
                    if (matrix[i + 1][j] == HIGHWAY) {
                        down = true;
                        dirs++;
                    } else if (matrix[i + 1][j] == ROAD) {
                        adjDown = true;
                        adjDirs++;
                    }
                }
                if (j != CITYS - 1) {
                    if (matrix[i][j + 1] == HIGHWAY) {
                        right = true;
                        dirs++;
                    } else if (matrix[i][j + 1] == ROAD) {
                        adjRight = true;
                        adjDirs++;
                    }
                }
                float X = -((float)(i - CITYS / 2)) / 2 - streetLength;
                float Z = ((float)(j - CITYS / 2)) / 2;
                if (dirs == 1) {
                    if (left) {
                        strs.CreateSimpleStreet(LEFT, glm::vec3(X, H, Z), streetLength, highwayWidth, highwayWidth);
                        roadMap[i][j].left = HIGHWAY;
                        roadMap[i][j].right = NOTHING;
                        roadMap[i][j].up = NOTHING;
                        roadMap[i][j].down = NOTHING;
                        roadMap[i][j].counter = 1;
                    } else if (right) {
                        strs.CreateSimpleStreet(RIGHT, glm::vec3(X, H, Z), streetLength, highwayWidth, highwayWidth);
                        roadMap[i][j].left = NOTHING;
                        roadMap[i][j].right = HIGHWAY;
                        roadMap[i][j].up = NOTHING;
                        roadMap[i][j].down = NOTHING;
                        roadMap[i][j].counter = 1;
                    } else if (up) {
                        strs.CreateSimpleStreet(UP, glm::vec3(X, H, Z), streetLength, highwayWidth, highwayWidth);
                        roadMap[i][j].left = NOTHING;
                        roadMap[i][j].right = NOTHING;
                        roadMap[i][j].up = HIGHWAY;
                        roadMap[i][j].down = NOTHING;
                        roadMap[i][j].counter = 1;
                    } else if (down) {
                        strs.CreateSimpleStreet(DOWN, glm::vec3(X, H, Z), streetLength, highwayWidth, highwayWidth);
                        roadMap[i][j].left = NOTHING;
                        roadMap[i][j].right = NOTHING;
                        roadMap[i][j].up = NOTHING;
                        roadMap[i][j].down = HIGHWAY;
                        roadMap[i][j].counter = 1;
                    }
                } else if (dirs >= 2) {
                    if (left) {
                        strs.CreateMeshStreet(LEFT, glm::vec3(X, H, Z), streetLength, highwayWidth, highwayWidth);
                        roadMap[i][j].left = HIGHWAY;
                        roadMap[i][j].counter++;
                    } else {
                        roadMap[i][j].left = NOTHING;
                    }
                    if (right) {
                        strs.CreateMeshStreet(RIGHT, glm::vec3(X, H, Z), streetLength, highwayWidth, highwayWidth);
                        roadMap[i][j].right = HIGHWAY;
                        roadMap[i][j].counter++;
                    } else {
                        roadMap[i][j].right = NOTHING;
                    }
                    if (up) {
                        strs.CreateMeshStreet(UP, glm::vec3(X, H, Z), streetLength, highwayWidth, highwayWidth);
                        roadMap[i][j].up = HIGHWAY;
                        roadMap[i][j].counter++;
                    } else {
                        roadMap[i][j].up = NOTHING;
                    }
                    if (down) {
                        strs.CreateMeshStreet(DOWN, glm::vec3(X, H, Z), streetLength, highwayWidth, highwayWidth);
                        roadMap[i][j].down = HIGHWAY;
                        roadMap[i][j].counter++;
                    } else {
                        roadMap[i][j].down = NOTHING;
                    }
                    strs.CreateMeshStreet(CENTER, glm::vec3(X, H, Z), streetLength, highwayWidth, highwayWidth);
                }
                if (adjDirs != 0) {
                    if (adjLeft) {
                        strs.CreateSimpleStreet(LEFT, glm::vec3(X, H, Z), streetLength, roadWidth, highwayWidth);
                        roadMap[i][j].left = ROAD;
                        roadMap[i][j].counter++;
                    }
                    if (adjRight) {
                        strs.CreateSimpleStreet(RIGHT, glm::vec3(X, H, Z), streetLength, roadWidth, highwayWidth);
                        roadMap[i][j].right = ROAD;
                        roadMap[i][j].counter++;
                    }
                    if (adjUp) {
                        strs.CreateSimpleStreet(UP, glm::vec3(X, H, Z), streetLength, roadWidth, highwayWidth);
                        roadMap[i][j].up = ROAD;
                        roadMap[i][j].counter++;
                    }
                    if (adjDown) {
                        strs.CreateSimpleStreet(DOWN, glm::vec3(X, H, Z), streetLength, roadWidth, highwayWidth);
                        roadMap[i][j].down = ROAD;
                        roadMap[i][j].counter++;
                    }
                }
            }
        }
    }
    std::vector<glm::vec3> normals;
    for (int i = 0; i < strs.vertices.size(); i++) {
        normals.push_back(glm::vec3(0, 1, 0));
    }
    std::vector<glm::vec2> textureCoords = {
        glm::vec2(0.0f, 0.0f),
        glm::vec2(CITYS, 0.0f),
        glm::vec2(CITYS, CITYS),
        glm::vec2(0.0f, CITYS)
    };
    strs.end(textureCoords, normals);
}

void Tema3::CreateComplexMeshes() {
    int indexw = 0, indexc = 0, indexp = 0;
    float delta = 50.0 / CITYS;
    float X = 25;
    float Z = -25;
    std::vector<VertexFormat> verticesw;
    std::vector<unsigned short> indicesw;
    std::vector<glm::vec3> verticesp;
    std::vector<unsigned short> indicesp;
    std::vector<glm::vec3> verticesc;
    std::vector<unsigned short> indicesc;
    std::vector<glm::vec2> textureCoordsp;
    std::vector<glm::vec2> textureCoordsc;
    glm::vec3 color = glm::vec3(0, 0, 1);
    bool ruw, luw, ldw, rdw,
        rup, lup, ldp, rdp,
        ruc, luc, ldc, rdc;
    for (int i = 0; i < waterComplex.size; i++) {
        for (int j = 0; j < waterComplex.size; j++) {
            float X2 = -((float)SCITYS / 50) * X + (float)SCITYS / 2;
            float Z2 = ((float)SCITYS / 50) * Z + (float)SCITYS / 2;
            ruw = false;
            luw = false;
            ldw = false;
            rdw = false;
            ruc = false;
            luc = false;
            ldc = false;
            rdc = false;
            rup = false;
            lup = false;
            ldp = false;
            rdp = false;
            if (i > 0) {
                if (j < CITYS) {
                    if (matrix[i - 1][j] == WATER) {
                        ruw = true;
                    } else if (matrix[i - 1][j] == PARK) {
                        rup = true;
                    } else if (matrix[i - 1][j] == BIGB) {
                        ruc = true;
                    }
                }
                if (j > 0) {
                    if (matrix[i - 1][j - 1] == WATER) {
                        luw = true;
                    } else if (matrix[i - 1][j - 1] == PARK) {
                        lup = true;
                    } else if (matrix[i - 1][j - 1] == BIGB) {
                        luc = true;
                    }
                }
            }
            if (i < CITYS) {
                if (j > 0) {
                    if (matrix[i][j - 1] == WATER) {
                        ldw = true;
                    } else if (matrix[i][j - 1] == PARK) {
                        lup = true;
                    } else if (matrix[i][j - 1] == BIGB) {
                        luc = true;
                    }
                }
                if (j < CITYS) {
                    if (matrix[i][j] == WATER) {
                        rdw = true;
                    } else if (matrix[i][j] == PARK) {
                        rdp = true;
                    } else if (matrix[i][j] == BIGB) {
                        rdc = true;
                    }
                }
            }
            if (ruw || luw || ldw || rdw) {
                waterComplex.matrix[i][j].value = indexw;
                indexw++;
                if (ruw || luw) {
                    waterComplex.matrix[i][j].up = true;
                }
                if (rdw || ldw) {
                    waterComplex.matrix[i][j].down = true;
                }
                if (ruw || rdw) {
                    waterComplex.matrix[i][j].right = true;
                }
                if (luw || ldw) {
                    waterComplex.matrix[i][j].left = true;
                }
                verticesw.push_back(VertexFormat(glm::vec3(X, 0.2, Z), color));
            } 
            if (ruc || luc || ldc || rdc) {
                compoundComplex.matrix[i][j].value = indexc;
                indexc++;
                if (ruc || luc) {
                    compoundComplex.matrix[i][j].up = true;
                }
                if (rdc || ldc) {
                    compoundComplex.matrix[i][j].down = true;
                }
                if (ruc || rdc) {
                    compoundComplex.matrix[i][j].right = true;
                }
                if (luc || ldc) {
                    compoundComplex.matrix[i][j].left = true;
                }
                verticesc.push_back(glm::vec3(X, 0.2, Z));
                textureCoordsc.push_back(glm::vec2(X2, Z2));
            } 
            if (rup || lup || ldp || rdp) {
                parkComplex.matrix[i][j].value = indexp;
                indexp++;
                if (rup || lup) {
                    parkComplex.matrix[i][j].up = true;
                }
                if (rdp || ldp) {
                    parkComplex.matrix[i][j].down = true;
                }
                if (rup || rdp) {
                    parkComplex.matrix[i][j].right = true;
                }
                if (lup || ldp) {
                    parkComplex.matrix[i][j].left = true;
                }
                verticesp.push_back(glm::vec3(X, 0.2, Z));
                textureCoordsp.push_back(glm::vec2(X2, Z2));
            }
            Z += delta;
        }
        X -= delta;
        Z = -25;
    }
    for (int i = 0; i < waterComplex.size - 1; i++) {
        for (int j = 0; j < waterComplex.size - 1; j++) {
            if (waterComplex.matrix[i][j].value != -1
                && waterComplex.matrix[i][j].right
                && waterComplex.matrix[i][j].down) {
                if (waterComplex.matrix[i + 1][j + 1].value != -1
                    && waterComplex.matrix[i + 1][j + 1].up
                    && waterComplex.matrix[i + 1][j + 1].left) {

                    indicesw.push_back(waterComplex.matrix[i    ][j    ].value);
                    indicesw.push_back(waterComplex.matrix[i    ][j + 1].value);
                    indicesw.push_back(waterComplex.matrix[i + 1][j    ].value);
                    indicesw.push_back(waterComplex.matrix[i + 1][j    ].value);
                    indicesw.push_back(waterComplex.matrix[i    ][j + 1].value);
                    indicesw.push_back(waterComplex.matrix[i + 1][j + 1].value);
                }
            }
            if (compoundComplex.matrix[i][j].value != -1
                && compoundComplex.matrix[i][j].right
                && compoundComplex.matrix[i][j].down) {
                if (compoundComplex.matrix[i + 1][j + 1].value != -1
                    && compoundComplex.matrix[i + 1][j + 1].up
                    && compoundComplex.matrix[i + 1][j + 1].left) {
                    indicesc.push_back(compoundComplex.matrix[i][j].value);
                    indicesc.push_back(compoundComplex.matrix[i][j + 1].value);
                    indicesc.push_back(compoundComplex.matrix[i + 1][j].value);
                    indicesc.push_back(compoundComplex.matrix[i + 1][j].value);
                    indicesc.push_back(compoundComplex.matrix[i][j + 1].value);
                    indicesc.push_back(compoundComplex.matrix[i + 1][j + 1].value);
                }
            }
            if (parkComplex.matrix[i][j].value != -1
                && parkComplex.matrix[i][j].right
                && parkComplex.matrix[i][j].down) {
                if (parkComplex.matrix[i + 1][j + 1].value != -1
                    && parkComplex.matrix[i + 1][j + 1].up
                    && parkComplex.matrix[i + 1][j + 1].left) {
                    indicesp.push_back(parkComplex.matrix[i    ][j    ].value);
                    indicesp.push_back(parkComplex.matrix[i    ][j + 1].value);
                    indicesp.push_back(parkComplex.matrix[i + 1][j    ].value);
                    indicesp.push_back(parkComplex.matrix[i + 1][j    ].value);
                    indicesp.push_back(parkComplex.matrix[i    ][j + 1].value);
                    indicesp.push_back(parkComplex.matrix[i + 1][j + 1].value);
                }
            }
        }
    }
    std::vector<glm::vec3> normalsp;
    for (int i = 0; i < verticesp.size(); i++) {
        normalsp.push_back(glm::vec3(0, 1, 0));
    }

    std::vector<glm::vec3> normalsc;
    for (int i = 0; i < verticesc.size(); i++) {
        normalsc.push_back(glm::vec3(0, 1, 0));
    }

    waterComplex.mesh->InitFromData(verticesw, indicesw);
    parkComplex.mesh->InitFromData(verticesp, normalsp, textureCoordsp, indicesp);
    compoundComplex.mesh->InitFromData(verticesc, normalsc, textureCoordsc, indicesc);
}

void Tema3::CreateBuildingsSideWalks() {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec2> textureCoords;
    std::vector<unsigned short> indices;
    int index = 0;
    float delta = 50.0 / CITYS;
    float X = 25 - delta;
    float Z = -25;
    float H = 0.2;
    float X2, Z2;
    for (int i = 0; i < CITYS; i++) {
        for (int j = 0; j < CITYS; j++) {
            if (roadMap[i][j].counter != 0) {
                if (index + 12 > USHORT_MAX) {
                    std::vector<glm::vec3> normals;
                    for (int i = 0; i < vertices.size(); i++) {
                        normals.push_back(glm::vec3(0, 1, 0));
                    }
                    pavements.back()->InitFromData(vertices, normals, textureCoords, indices);
                    vertices.clear();
                    textureCoords.clear();
                    indices.clear();
                    index = 0;
                    pavements.push_back(new Mesh("pavements"));
                }
                bool todo = false; // left down corner
                if (i != CITYS
                    && j != 0) {
                    if (matrix[i + 1][j - 1] != WATER) {
                        todo = true;
                    }
                }
                if (todo) {
                    if (roadMap[i][j].left == ROAD
                        && roadMap[i][j].down == ROAD) {
                        vertices.push_back(glm::vec3(X, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + (streetLength - roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X, H, Z + (streetLength - roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    } else if (roadMap[i][j].left == HIGHWAY
                        && roadMap[i][j].down == HIGHWAY) {
                        vertices.push_back(glm::vec3(X, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X, H, Z + (streetLength - highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    } else if (roadMap[i][j].left == ROAD
                        && roadMap[i][j].down == HIGHWAY) {
                        vertices.push_back(glm::vec3(X, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X, H, Z + (streetLength - highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    } else if (roadMap[i][j].left == HIGHWAY
                        && roadMap[i][j].down == ROAD) {
                        vertices.push_back(glm::vec3(X, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + (streetLength - roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X, H, Z + (streetLength - roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    }
                }
                todo = false; // left up corner
                if (i != 0
                    && j != 0) {
                    if (matrix[i - 1][j - 1] != WATER) {
                        todo = true;
                    }
                }
                if (todo) {
                    if (roadMap[i][j].left == ROAD
                        && roadMap[i][j].up == ROAD) {
                        vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + (streetLength - roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    } else if (roadMap[i][j].left == HIGHWAY
                        && roadMap[i][j].up == HIGHWAY) {
                        vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    } else if (roadMap[i][j].left == ROAD
                        && roadMap[i][j].up == HIGHWAY) {
                        vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    } else if (roadMap[i][j].left == HIGHWAY
                        && roadMap[i][j].up == ROAD) {
                        vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength - roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    }
                }
                todo = false; // right up corner
                if (i != 0
                    && j != CITYS - 1) {
                    if (matrix[i - 1][j + 1] != WATER) {
                        todo = true;
                    }
                }
                if (todo) {
                    if (roadMap[i][j].up == ROAD
                        && roadMap[i][j].right == ROAD) {
                        vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + (streetLength + roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength + roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    } else if (roadMap[i][j].up == HIGHWAY
                        && roadMap[i][j].right == HIGHWAY) {
                        vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength + highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    } else if (roadMap[i][j].up == ROAD
                        && roadMap[i][j].right == HIGHWAY) {
                        vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength + roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength + roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    } else if (roadMap[i][j].up == HIGHWAY
                        && roadMap[i][j].right == ROAD) {
                        vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength + highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    }
                }
                todo = false; // right down corner
                if (i != CITYS - 1
                    && j != CITYS - 1) {
                    if (matrix[i + 1][j + 1] != WATER) {
                        todo = true;
                    }
                }
                if (todo) {
                    if (roadMap[i][j].down == ROAD
                        && roadMap[i][j].right == ROAD) {
                        vertices.push_back(glm::vec3(X, H, Z + (streetLength + roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + (streetLength + roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    } else if (roadMap[i][j].down == HIGHWAY
                        && roadMap[i][j].right == HIGHWAY) {
                        vertices.push_back(glm::vec3(X, H, Z + (streetLength + highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    } else if (roadMap[i][j].down == ROAD
                        && roadMap[i][j].right == HIGHWAY) {
                        vertices.push_back(glm::vec3(X, H, Z + (streetLength + roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + (streetLength + roadWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    } else if (roadMap[i][j].down == HIGHWAY
                        && roadMap[i][j].right == ROAD) {
                        vertices.push_back(glm::vec3(X, H, Z + (streetLength + highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        vertices.push_back(glm::vec3(X, H, Z + streetLength));
                        X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                        Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                        textureCoords.push_back(glm::vec2(X2, Z2));
                        indices.push_back(index);
                        indices.push_back(index + 1);
                        indices.push_back(index + 2);
                        indices.push_back(index);
                        indices.push_back(index + 2);
                        indices.push_back(index + 3);
                        index += 4;
                    }
                }
                if (roadMap[i][j].counter == 3) {
                    todo = false; // right side
                    if (j != CITYS - 1) {
                        if (matrix[i][j + 1] != WATER) {
                            todo = true;
                        }
                    }
                    if (todo && roadMap[i][j].right == NOTHING) {
                        if (roadMap[i][j].up == ROAD
                            && roadMap[i][j].down == ROAD) {
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        } else if (roadMap[i][j].up == HIGHWAY
                            && roadMap[i][j].down == HIGHWAY) {
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        } else if (roadMap[i][j].up == HIGHWAY
                            && roadMap[i][j].down == ROAD) {
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        } else if (roadMap[i][j].up == ROAD
                            && roadMap[i][j].down == HIGHWAY) {
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        }
                            
                    }
                    todo = false; // left side
                    if (j != 0) {
                        if (matrix[i][j - 1] != WATER) {
                            todo = true;
                        }
                    }
                    if (todo && roadMap[i][j].left == NOTHING) {
                        if (roadMap[i][j].up == ROAD
                            && roadMap[i][j].down == ROAD) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        } else if (roadMap[i][j].up == HIGHWAY
                            && roadMap[i][j].down == HIGHWAY) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        } else if (roadMap[i][j].up == HIGHWAY
                            && roadMap[i][j].down == ROAD) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        } else if (roadMap[i][j].up == ROAD
                            && roadMap[i][j].down == HIGHWAY) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        }
                    }
                    todo = false; // up
                    if (i != 0) {
                        if (matrix[i - 1][j] != WATER) {
                            todo = true;
                        }
                    }
                    if (roadMap[i][j].up == NOTHING && todo) {
                        if (roadMap[i][j].left == ROAD
                            && roadMap[i][j].right == ROAD) {
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        } else if (roadMap[i][j].left == HIGHWAY
                            && roadMap[i][j].right == HIGHWAY) {
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        } else if (roadMap[i][j].left == ROAD
                            && roadMap[i][j].right == HIGHWAY) {
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        } else if (roadMap[i][j].left == HIGHWAY
                            && roadMap[i][j].right == ROAD) {
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        }
                    }
                    todo = false; // down
                    if (i != CITYS - 1) {
                        if (matrix[i + 1][j] != WATER) {
                            todo = true;
                        }
                    }
                    if (roadMap[i][j].down == NOTHING && todo) {
                        if (roadMap[i][j].left == ROAD
                            && roadMap[i][j].right == ROAD) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        } else if (roadMap[i][j].left == HIGHWAY
                            && roadMap[i][j].right == HIGHWAY) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        } else if (roadMap[i][j].left == ROAD
                            && roadMap[i][j].right == HIGHWAY) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        } else if (roadMap[i][j].left == HIGHWAY
                            && roadMap[i][j].right == ROAD) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        }
                    }
                } else if (roadMap[i][j].counter == 2) {
                    bool todo1, todo2;
                    todo1 = false;
                    todo2 = false;
                    if (i != 0) { // up
                        if (matrix[i - 1][j] != WATER) {
                            todo1 = true;
                        }
                    }
                    if (i != CITYS - 1) { // down
                        if (matrix[i + 1][j] != WATER) {
                            todo2 = true;
                        }
                    }
                    if (roadMap[i][j].right != NOTHING
                        && roadMap[i][j].left != NOTHING) {
                        if (todo1) {
                            if (roadMap[i][j].left == ROAD
                                && roadMap[i][j].right == ROAD) {
                                vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + streetLength, H, Z));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + streetLength));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                indices.push_back(index);
                                indices.push_back(index + 1);
                                indices.push_back(index + 2);
                                indices.push_back(index);
                                indices.push_back(index + 2);
                                indices.push_back(index + 3);
                                index += 4;
                            } else if (roadMap[i][j].left == HIGHWAY
                                && roadMap[i][j].right == HIGHWAY) {
                                vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + streetLength, H, Z));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + streetLength));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                indices.push_back(index);
                                indices.push_back(index + 1);
                                indices.push_back(index + 2);
                                indices.push_back(index);
                                indices.push_back(index + 2);
                                indices.push_back(index + 3);
                                index += 4;
                            }
                        }
                        if (todo2) {
                            if (roadMap[i][j].left == ROAD
                                && roadMap[i][j].right == ROAD) {
                                vertices.push_back(glm::vec3(X, H, Z));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + streetLength));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X, H, Z + streetLength));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                indices.push_back(index);
                                indices.push_back(index + 1);
                                indices.push_back(index + 2);
                                indices.push_back(index);
                                indices.push_back(index + 2);
                                indices.push_back(index + 3);
                                index += 4;
                            } else if (roadMap[i][j].left == HIGHWAY
                                && roadMap[i][j].right == HIGHWAY) {
                                vertices.push_back(glm::vec3(X, H, Z));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + streetLength));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X, H, Z + streetLength));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                indices.push_back(index);
                                indices.push_back(index + 1);
                                indices.push_back(index + 2);
                                indices.push_back(index);
                                indices.push_back(index + 2);
                                indices.push_back(index + 3);
                                index += 4;
                            }
                        }
                    }
                    todo1 = false;
                    todo2 = false;
                    if (j != 0) {
                        if (matrix[i][j - 1] != WATER) {
                            todo1 = true; // left
                        }
                    }
                    if (j != CITYS - 1) {
                        if (matrix[i][j + 1] != WATER) {
                            todo2 = true; // right
                        }
                    }
                    if (roadMap[i][j].up != NOTHING
                        && roadMap[i][j].down != NOTHING) {
                        if (todo1) {
                            if (roadMap[i][j].up == ROAD
                                && roadMap[i][j].down == ROAD) {
                                vertices.push_back(glm::vec3(X, H, Z));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + streetLength, H, Z));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - roadWidth) / 2));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X, H, Z + (streetLength - roadWidth) / 2));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                indices.push_back(index);
                                indices.push_back(index + 1);
                                indices.push_back(index + 2);
                                indices.push_back(index);
                                indices.push_back(index + 2);
                                indices.push_back(index + 3);
                                index += 4;
                            } else if (roadMap[i][j].up == HIGHWAY
                                && roadMap[i][j].down == HIGHWAY) {
                                vertices.push_back(glm::vec3(X, H, Z));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + streetLength, H, Z));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - highwayWidth) / 2));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X, H, Z + (streetLength - highwayWidth) / 2));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                indices.push_back(index);
                                indices.push_back(index + 1);
                                indices.push_back(index + 2);
                                indices.push_back(index);
                                indices.push_back(index + 2);
                                indices.push_back(index + 3);
                                index += 4;
                            }
                        }
                        if (todo2) {
                            if (roadMap[i][j].up == ROAD
                                && roadMap[i][j].down == ROAD) {
                                vertices.push_back(glm::vec3(X, H, Z + (streetLength + roadWidth) / 2));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength + roadWidth) / 2));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X, H, Z + streetLength));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                indices.push_back(index);
                                indices.push_back(index + 1);
                                indices.push_back(index + 2);
                                indices.push_back(index);
                                indices.push_back(index + 2);
                                indices.push_back(index + 3);
                                index += 4;
                            } else if (roadMap[i][j].up == HIGHWAY
                                && roadMap[i][j].down == HIGHWAY) {
                                vertices.push_back(glm::vec3(X, H, Z + (streetLength + highwayWidth) / 2));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength + highwayWidth) / 2));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                vertices.push_back(glm::vec3(X, H, Z + streetLength));
                                X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                                Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                                textureCoords.push_back(glm::vec2(X2, Z2));
                                indices.push_back(index);
                                indices.push_back(index + 1);
                                indices.push_back(index + 2);
                                indices.push_back(index);
                                indices.push_back(index + 2);
                                indices.push_back(index + 3);
                                index += 4;
                            }
                        }
                    }
                    todo = false; // left down corner
                    if (i != CITYS - 1
                        && j != 0) {
                        if (matrix[i + 1][j - 1] != WATER) {
                            if (matrix[i][j - 1] != WATER) {
                                if (matrix[i + 1][j] != WATER) {
                                    todo = true;
                                }
                            }
                        }
                    }
                    if (todo) {
                        if (roadMap[i][j].up == ROAD
                            && roadMap[i][j].right == ROAD) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                        } else if (roadMap[i][j].up == HIGHWAY
                            && roadMap[i][j].right == HIGHWAY) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        }
                    }
                    todo = false; // left up corner
                    if (i != 0
                        && j != 0) {
                        if (matrix[i - 1][j - 1] != WATER) {
                            if (matrix[i][j - 1] != WATER) {
                                if (matrix[i - 1][j] != WATER) {
                                    todo = true;
                                }
                            }
                        }
                    }
                    if (todo) {
                        if (roadMap[i][j].right == ROAD
                            && roadMap[i][j].down == ROAD) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        } else if (roadMap[i][j].right == HIGHWAY
                            && roadMap[i][j].down == HIGHWAY) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        }
                    }
                    todo = false; // right up corner
                    if (i != 0
                        && j != CITYS - 1) {
                        if (matrix[i - 1][j + 1] != WATER) {
                            if (matrix[i - 1][j] != WATER) {
                                if (matrix[i][j + 1] != WATER) {
                                    todo = true;
                                }
                            }
                        }
                    }
                    if (todo) {
                        if (roadMap[i][j].left == ROAD
                            && roadMap[i][j].down == ROAD) {
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        } else if (roadMap[i][j].left == HIGHWAY
                            && roadMap[i][j].down == HIGHWAY) {
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        }
                    }
                    todo = false; // right down corner
                    if (i != CITYS - 1
                        && j != CITYS - 1) {
                        if (matrix[i + 1][j + 1] != WATER) {
                            if (matrix[i][j + 1] != WATER) {
                                if (matrix[i + 1][j] != WATER) {
                                    todo = true;
                                }
                            }
                        }
                    }
                    if (todo) {
                        if (roadMap[i][j].left == ROAD
                            && roadMap[i][j].up == ROAD) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        } else if (roadMap[i][j].left == HIGHWAY
                            && roadMap[i][j].up == HIGHWAY) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        }
                    }
                } else if (roadMap[i][j].counter == 1) {
                    if (matrix[i][j] == ROAD) {
                        todo = false; // up
                        if (i != 0) {
                            if (matrix[i - 1][j] != WATER) {
                                if (j != 0) {
                                    if (matrix[i][j - 1] != WATER) {
                                        if (j != CITYS - 1) {
                                            if (matrix[i][j + 1] != WATER) {
                                                todo = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (todo && roadMap[i][j].up != NOTHING) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength + highwayWidth) / 2, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        }
                        todo = false; // down
                        if (i != CITYS - 1 && j != 0 && j != CITYS + 1) {
                            if (matrix[i + 1][j] != WATER) {
                                if (matrix[i][j + 1] != WATER) {
                                    if (matrix[i][j - 1] != WATER) {
                                        todo = true;
                                    }
                                }
                            }
                        }
                        if (todo && roadMap[i][j].down != NOTHING) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + (streetLength - roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - highwayWidth) / 2, H, Z + (streetLength + roadWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        }
                        todo = false; // left
                        if (j != 0 && i != 0 && i != CITYS - 1) {
                            if (matrix[i][j - 1] != WATER) {
                                if (matrix[i + 1][j] != WATER) {
                                    if (matrix[i - 1][j] != WATER) {
                                        todo = true;
                                    }
                                }
                            }
                        }
                        if (todo && roadMap[i][j].left != NOTHING) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + (streetLength - highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        }
                        todo = false; // right
                        if (j != CITYS - 1 && i != 0 && i != CITYS - 1) {
                            if (matrix[i][j + 1] != WATER) {
                                if (matrix[i + 1][j] != WATER) {
                                    if (matrix[i - 1][j] != WATER) {
                                        todo = true;
                                    }
                                }
                            }
                        }
                        if (todo && roadMap[i][j].right != NOTHING) {
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X, H, Z +streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength - roadWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + (streetLength + highwayWidth) / 2));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + streetLength, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            vertices.push_back(glm::vec3(X + (streetLength + roadWidth) / 2, H, Z + streetLength));
                            X2 = -((float)SCITYS / 50) * vertices.back().x + (float)SCITYS / 2;
                            Z2 = ((float)SCITYS / 50) * vertices.back().z + (float)SCITYS / 2;
                            textureCoords.push_back(glm::vec2(X2, Z2));
                            indices.push_back(index);
                            indices.push_back(index + 1);
                            indices.push_back(index + 2);
                            indices.push_back(index);
                            indices.push_back(index + 2);
                            indices.push_back(index + 3);
                            index += 4;
                        }
                    }
                }
            }
            Z += delta;
        }
        X -= delta;
        Z = -25;
    }

    std::vector<glm::vec3> normals;
    for (int i = 0; i < vertices.size(); i++) {
        normals.push_back(glm::vec3(0, 1, 0));
    }
    pavements.back()->InitFromData(vertices, normals, textureCoords, indices);
}

void Tema3::CreateTriangleBuilding(glm::vec3 corner) {
    float X = corner.x;
    float H = corner.y;
    float Z = corner.z;
    float X2, Z2;
    float mb1h = meter * MB1H,
        mb1w = meter * MB1W,
        mb2h = meter * MB2H,
        mb2w = meter * MB2W,
        ob1h = meter * OB1H,
        ob1w = meter * OB1W;
    int indicesToAdd = 24;
    if (indexMB1 + indicesToAdd > USHORT_MAX) {
        buildingsMB1.back()->InitFromData(verticesMB1, normalsMB1, textCoordsMB1, indicesMB1);
        buildingsMB1.push_back(new Mesh("buildings"));
        verticesMB1.clear();
        normalsMB1.clear();
        textCoordsMB1.clear();
        indicesMB1.clear();
        indexMB1 = 0;
    }
    // sides of the first building
    float h = rand() % 5 + 5;
    h *= mb1h;
    verticesMB1.push_back(glm::vec3(X + 4 * meter, H, Z));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(0, 0, -1));
    verticesMB1.push_back(glm::vec3(X + 64 * meter, H, Z));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(0, 0, -1));
    verticesMB1.push_back(glm::vec3(X + 4 * meter, H + h, Z));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(0, 0, -1));
    verticesMB1.push_back(glm::vec3(X + 64 * meter, H + h, Z));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(0, 0, -1));
    indicesMB1.push_back(indexMB1);
    indicesMB1.push_back(indexMB1 + 1);
    indicesMB1.push_back(indexMB1 + 2);
    indicesMB1.push_back(indexMB1 + 1);
    indicesMB1.push_back(indexMB1 + 2);
    indicesMB1.push_back(indexMB1 + 3);
    indexMB1 += 4;

    verticesMB1.push_back(glm::vec3(X + 64 * meter, H, Z));
    X2 = -(maxMB1 / 50) * verticesMB1.back().z + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(1, 0, 0));
    verticesMB1.push_back(glm::vec3(X + 64 * meter, H, Z + 60 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().z + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(1, 0, 0));
    verticesMB1.push_back(glm::vec3(X + 64 * meter, H + h, Z));
    X2 = -(maxMB1 / 50) * verticesMB1.back().z + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(1, 0, 0));
    verticesMB1.push_back(glm::vec3(X + 64 * meter, H + h, Z + 60 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().z + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(1, 0, 0));
    indicesMB1.push_back(indexMB1);
    indicesMB1.push_back(indexMB1 + 1);
    indicesMB1.push_back(indexMB1 + 2);
    indicesMB1.push_back(indexMB1 + 1);
    indicesMB1.push_back(indexMB1 + 2);
    indicesMB1.push_back(indexMB1 + 3);
    indexMB1 += 4;

    verticesMB1.push_back(glm::vec3(X + 4 * meter, H, Z));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(-1, 0, 1));
    verticesMB1.push_back(glm::vec3(X + 64 * meter, H, Z + 60 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(-1, 0, 1));
    verticesMB1.push_back(glm::vec3(X + 4 * meter, H + h, Z));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(-1, 0, 1));
    verticesMB1.push_back(glm::vec3(X + 64 * meter, H + h, Z + 60 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(-1, 0, 1));
    indicesMB1.push_back(indexMB1);
    indicesMB1.push_back(indexMB1 + 1);
    indicesMB1.push_back(indexMB1 + 2);
    indicesMB1.push_back(indexMB1 + 1);
    indicesMB1.push_back(indexMB1 + 2);
    indicesMB1.push_back(indexMB1 + 3);
    indexMB1 += 4;

    // roof of the first building
    if (indexRoofs + 3 >= USHORT_MAX) {
        roofs.back()->InitFromData(verticesRoofs, normalsRoofs, textCoordsRoofs, indicesRoofs);
        verticesRoofs.clear();
        normalsRoofs.clear();
        textCoordsRoofs.clear();
        indicesRoofs.clear();
        roofs.push_back(new Mesh("roofs"));
        indexRoofs = 0;
    }
    verticesRoofs.push_back(glm::vec3(X + 4 * meter, H + h, Z));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    verticesRoofs.push_back(glm::vec3(X + 64 * meter, H + h, Z));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    verticesRoofs.push_back(glm::vec3(X + 64 * meter, H + h, Z + 60 * meter));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    indicesRoofs.push_back(indexRoofs);
    indicesRoofs.push_back(indexRoofs + 1);
    indicesRoofs.push_back(indexRoofs + 2);
    indexRoofs += 3;

    // sides of the second building
    h = rand() % 5 + 5;
    h *= mb1h;
    verticesMB1.push_back(glm::vec3(X, H, Z + 4 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().z + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(-1, 0, 0));
    verticesMB1.push_back(glm::vec3(X, H, Z + 64 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().z + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(-1, 0, 0));
    verticesMB1.push_back(glm::vec3(X, H + h, Z + 4 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().z + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(-1, 0, 0));
    verticesMB1.push_back(glm::vec3(X, H + h, Z + 64 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().z + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(-1, 0, 0));
    indicesMB1.push_back(indexMB1);
    indicesMB1.push_back(indexMB1 + 1);
    indicesMB1.push_back(indexMB1 + 2);
    indicesMB1.push_back(indexMB1 + 1);
    indicesMB1.push_back(indexMB1 + 2);
    indicesMB1.push_back(indexMB1 + 3);
    indexMB1 += 4;

    verticesMB1.push_back(glm::vec3(X, H, Z + 64 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(0, 0, 1));
    verticesMB1.push_back(glm::vec3(X + 60 * meter, H, Z + 64 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(0, 0, 1));
    verticesMB1.push_back(glm::vec3(X, H + h, Z + 64 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(0, 0, 1));
    verticesMB1.push_back(glm::vec3(X + 60 * meter, H + h, Z + 64 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(0, 0, 1));
    indicesMB1.push_back(indexMB1);
    indicesMB1.push_back(indexMB1 + 1);
    indicesMB1.push_back(indexMB1 + 2);
    indicesMB1.push_back(indexMB1 + 1);
    indicesMB1.push_back(indexMB1 + 2);
    indicesMB1.push_back(indexMB1 + 3);
    indexMB1 += 4;

    verticesMB1.push_back(glm::vec3(X, H, Z + 4 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(1, 0, -1));
    verticesMB1.push_back(glm::vec3(X + 60 * meter, H, Z + 64 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(1, 0, -1));
    verticesMB1.push_back(glm::vec3(X, H + h, Z + 4 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(1, 0, -1));
    verticesMB1.push_back(glm::vec3(X + 60 * meter, H + h, Z + 64 * meter));
    X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
    Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
    textCoordsMB1.push_back(glm::vec2(X2, Z2));
    normalsMB1.push_back(glm::vec3(1, 0, -1));
    indicesMB1.push_back(indexMB1);
    indicesMB1.push_back(indexMB1 + 1);
    indicesMB1.push_back(indexMB1 + 2);
    indicesMB1.push_back(indexMB1 + 1);
    indicesMB1.push_back(indexMB1 + 2);
    indicesMB1.push_back(indexMB1 + 3);
    indexMB1 += 4;

    if (indexRoofs + 3 >= USHORT_MAX) {
        roofs.back()->InitFromData(verticesRoofs, normalsRoofs, textCoordsRoofs, indicesRoofs);
        verticesRoofs.clear();
        normalsRoofs.clear();
        textCoordsRoofs.clear();
        indicesRoofs.clear();
        roofs.push_back(new Mesh("roofs"));
        indexRoofs = 0;
    }
    verticesRoofs.push_back(glm::vec3(X, H + h, Z + 4 * meter));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    verticesRoofs.push_back(glm::vec3(X, H + h, Z + 64 * meter));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    verticesRoofs.push_back(glm::vec3(X + 60 * meter, H + h, Z + 64 * meter));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    indicesRoofs.push_back(indexRoofs);
    indicesRoofs.push_back(indexRoofs + 1);
    indicesRoofs.push_back(indexRoofs + 2);
    indexRoofs += 3;
}

void Tema3::CreateCilinderBuilding(glm::vec3 corner) {
    float X = corner.x;
    float H = corner.y;
    float Z = corner.z;
    float X2, Z2;
    float mb1h = meter * MB1H,
        mb1w = meter * MB1W,
        mb2h = meter * MB2H,
        mb2w = meter * MB2W,
        ob1h = meter * OB1H,
        ob1w = meter * OB1W;
    float coef = 10;
    int indicesToAdd = 8 * coef + 8;
    if (indexMB1 + indicesToAdd > USHORT_MAX) {
        buildingsMB1.back()->InitFromData(verticesMB1, normalsMB1, textCoordsMB1, indicesMB1);
        buildingsMB1.push_back(new Mesh("buildings"));
        verticesMB1.clear();
        normalsMB1.clear();
        textCoordsMB1.clear();
        indicesMB1.clear();
        indexMB1 = 0;
    }

    float angle = glm::radians(45.0);
    float delta = glm::radians(180.0 / coef);
    float halfDelta = delta / 2;
    float radius = meter * 30;
    // first building sides
    float startX = X + meter * 30;
    float startZ = Z + meter * 34;
    for (int k = 0; k < 2; k++) {
        float h = rand() % 10 + 5;
        h *= mb1h;
        for (int i = 0; i < coef / 2; i++) {
            verticesMB1.push_back(glm::vec3(startX + radius * cos(angle), H, startZ + radius * sin(angle)));
            X2 = -(maxMB1 / 50) * verticesMB1.back().z + maxMB1 / 2;
            Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
            textCoordsMB1.push_back(glm::vec2(X2, Z2));
            normalsMB1.push_back(glm::vec3(sin(angle + halfDelta), 0, cos(angle + halfDelta)));
            verticesMB1.push_back(glm::vec3(startX + radius * cos(angle), H + h, startZ + radius * sin(angle)));
            X2 = -(maxMB1 / 50) * verticesMB1.back().z + maxMB1 / 2;
            Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
            textCoordsMB1.push_back(glm::vec2(X2, Z2));
            normalsMB1.push_back(normalsMB1.back());
            verticesMB1.push_back(glm::vec3(startX + radius * cos(angle + delta), H, startZ + radius * sin(angle + delta)));
            X2 = -(maxMB1 / 50) * verticesMB1.back().z + maxMB1 / 2;
            Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
            textCoordsMB1.push_back(glm::vec2(X2, Z2));
            normalsMB1.push_back(glm::vec3(sin(angle + delta + halfDelta), 0, cos(angle + delta + halfDelta)));
            verticesMB1.push_back(glm::vec3(startX + radius * cos(angle + delta), H + h, startZ + radius * sin(angle + delta)));
            X2 = -(maxMB1 / 50) * verticesMB1.back().z + maxMB1 / 2;
            Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
            textCoordsMB1.push_back(glm::vec2(X2, Z2));
            normalsMB1.push_back(normalsMB1.back());
            indicesMB1.push_back(indexMB1);
            indicesMB1.push_back(indexMB1 + 1);
            indicesMB1.push_back(indexMB1 + 2);
            indicesMB1.push_back(indexMB1 + 1);
            indicesMB1.push_back(indexMB1 + 2);
            indicesMB1.push_back(indexMB1 + 3);
            indexMB1 += 4;
            if (indexRoofs + 3 >= USHORT_MAX) {
                roofs.back()->InitFromData(verticesRoofs, normalsRoofs, textCoordsRoofs, indicesRoofs);
                verticesRoofs.clear();
                normalsRoofs.clear();
                textCoordsRoofs.clear();
                indicesRoofs.clear();
                roofs.push_back(new Mesh("roofs"));
                indexRoofs = 0;
            }
            verticesRoofs.push_back(glm::vec3(startX, H + h, startZ));
            X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
            Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
            textCoordsRoofs.push_back(glm::vec2(X2, Z2));
            normalsRoofs.push_back(glm::vec3(0, 1, 0));
            verticesRoofs.push_back(glm::vec3(startX + radius * cos(angle), H + h, startZ + radius * sin(angle)));
            X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
            Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
            textCoordsRoofs.push_back(glm::vec2(X2, Z2));
            normalsRoofs.push_back(glm::vec3(0, 1, 0));
            verticesRoofs.push_back(glm::vec3(startX + radius * cos(angle + delta), H + h, startZ + radius * sin(angle + delta)));
            X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
            Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
            textCoordsRoofs.push_back(glm::vec2(X2, Z2));
            normalsRoofs.push_back(glm::vec3(0, 1, 0));
            indicesRoofs.push_back(indexRoofs);
            indicesRoofs.push_back(indexRoofs + 1);
            indicesRoofs.push_back(indexRoofs + 2);
            indexRoofs += 3;
            angle += delta;
        }
        for (int i = coef / 2; i < coef; i++) {
            verticesMB1.push_back(glm::vec3(startX + radius * cos(angle), H, startZ + radius * sin(angle)));
            X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
            Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
            textCoordsMB1.push_back(glm::vec2(X2, Z2));
            normalsMB1.push_back(glm::vec3(sin(angle + halfDelta), 0, cos(angle + halfDelta)));
            verticesMB1.push_back(glm::vec3(startX + radius * cos(angle), H + h, startZ + radius * sin(angle)));
            X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
            Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
            textCoordsMB1.push_back(glm::vec2(X2, Z2));
            normalsMB1.push_back(normalsMB1.back());
            verticesMB1.push_back(glm::vec3(startX + radius * cos(angle + delta), H, startZ + radius * sin(angle + delta)));
            X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
            Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
            textCoordsMB1.push_back(glm::vec2(X2, Z2));
            normalsMB1.push_back(glm::vec3(sin(angle + delta + halfDelta), 0, cos(angle + delta + halfDelta)));
            verticesMB1.push_back(glm::vec3(startX + radius * cos(angle + delta), H + h, startZ + radius * sin(angle + delta)));
            X2 = -(maxMB1 / 50) * verticesMB1.back().x + maxMB1 / 2;
            Z2 = (maxMB1 / 50) * (verticesMB1.back().y - H) + maxMB1 / 2;
            textCoordsMB1.push_back(glm::vec2(X2, Z2));
            normalsMB1.push_back(normalsMB1.back());
            indicesMB1.push_back(indexMB1);
            indicesMB1.push_back(indexMB1 + 1);
            indicesMB1.push_back(indexMB1 + 2);
            indicesMB1.push_back(indexMB1 + 1);
            indicesMB1.push_back(indexMB1 + 2);
            indicesMB1.push_back(indexMB1 + 3);
            indexMB1 += 4;
            if (indexRoofs + 3 >= USHORT_MAX) {
                roofs.back()->InitFromData(verticesRoofs, normalsRoofs, textCoordsRoofs, indicesRoofs);
                verticesRoofs.clear();
                normalsRoofs.clear();
                textCoordsRoofs.clear();
                indicesRoofs.clear();
                roofs.push_back(new Mesh("roofs"));
                indexRoofs = 0;
            }
            verticesRoofs.push_back(glm::vec3(startX, H + h, startZ));
            X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
            Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
            textCoordsRoofs.push_back(glm::vec2(X2, Z2));
            normalsRoofs.push_back(glm::vec3(0, 1, 0));
            verticesRoofs.push_back(glm::vec3(startX + radius * cos(angle), H + h, startZ + radius * sin(angle)));
            X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
            Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
            textCoordsRoofs.push_back(glm::vec2(X2, Z2));
            normalsRoofs.push_back(glm::vec3(0, 1, 0));
            verticesRoofs.push_back(glm::vec3(startX + radius * cos(angle + delta), H + h, startZ + radius * sin(angle + delta)));
            X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
            Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
            textCoordsRoofs.push_back(glm::vec2(X2, Z2));
            normalsRoofs.push_back(glm::vec3(0, 1, 0));
            indicesRoofs.push_back(indexRoofs);
            indicesRoofs.push_back(indexRoofs + 1);
            indicesRoofs.push_back(indexRoofs + 2);
            indexRoofs += 3;
            angle += delta;
        }

        if (indexRoofs + 4 >= USHORT_MAX) {
            roofs.back()->InitFromData(verticesRoofs, normalsRoofs, textCoordsRoofs, indicesRoofs);
            verticesRoofs.clear();
            normalsRoofs.clear();
            textCoordsRoofs.clear();
            indicesRoofs.clear();
            roofs.push_back(new Mesh("roofs"));
            indexRoofs = 0;
        }
        verticesRoofs.push_back(glm::vec3(startX + radius * cos(angle - coef * delta), H, startZ + radius * sin(angle - coef * delta)));
        X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
        Z2 = ((float)SCITYS / 50) * verticesRoofs.back().y + (float)SCITYS / 2;
        textCoordsRoofs.push_back(glm::vec2(X2, Z2));
        verticesRoofs.push_back(glm::vec3(startX + radius * cos(angle - coef * delta), H + h, startZ + radius * sin(angle - coef * delta)));
        X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
        Z2 = ((float)SCITYS / 50) * verticesRoofs.back().y + (float)SCITYS / 2;
        textCoordsRoofs.push_back(glm::vec2(X2, Z2));
        verticesRoofs.push_back(glm::vec3(startX + radius * cos(angle), H, startZ + radius * sin(angle)));
        X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
        Z2 = ((float)SCITYS / 50) * verticesRoofs.back().y + (float)SCITYS / 2;
        textCoordsRoofs.push_back(glm::vec2(X2, Z2));
        verticesRoofs.push_back(glm::vec3(startX + radius * cos(angle), H + h, startZ + radius * sin(angle)));
        X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
        Z2 = ((float)SCITYS / 50) * verticesRoofs.back().y + (float)SCITYS / 2;
        textCoordsRoofs.push_back(glm::vec2(X2, Z2));
        normalsRoofs.push_back(glm::vec3(0, 1, 0));
        indicesRoofs.push_back(indexRoofs);
        indicesRoofs.push_back(indexRoofs + 1);
        indicesRoofs.push_back(indexRoofs + 2);
        indicesRoofs.push_back(indexRoofs + 1);
        indicesRoofs.push_back(indexRoofs + 2);
        indicesRoofs.push_back(indexRoofs + 3);
        indexRoofs += 4;
        if (k == 0) {
            for (int t = 0; t < 4; t++) {
                normalsRoofs.push_back(glm::vec3(-1, 0, 1));
            }
        } else if (k == 1) {
            for (int t = 0; t < 4; t++) {
                normalsRoofs.push_back(glm::vec3(1, 0, -1));
            }
        }

        angle = glm::radians(225.0);
        startX = X + meter * 34;
        startZ = Z + meter * 30;
    }
}

float Tema3::CreateLApartments(glm::vec3 corner) {
    float X = corner.x;
    float H = corner.y;
    float Z = corner.z;
    float X2, Z2;
    int indicesToAdd = 24;
    if (indicesToAdd + indexAP > USHORT_MAX) {
        buildingsAP.back()->InitFromData(verticesAP, normalsAP, textCoordsAP, indicesAP);
        verticesAP.clear();
        normalsAP.clear();
        textCoordsAP.clear();
        indicesAP.clear();
        indexAP = 0;
        buildingsAP.push_back(new Mesh("buildings"));
    }
    float ap1w = meter * AP1W;
    float ap1h = meter * AP1W;
    float h = rand() % 2 + 4;
    h *= ap1h;



    if (indexRoofs + 8 >= USHORT_MAX) {
        roofs.back()->InitFromData(verticesRoofs, normalsRoofs, textCoordsRoofs, indicesRoofs);
        verticesRoofs.clear();
        normalsRoofs.clear();
        textCoordsRoofs.clear();
        indicesRoofs.clear();
        roofs.push_back(new Mesh("roofs"));
        indexRoofs = 0;
    }
    verticesRoofs.push_back(corner + glm::vec3(0, h, 0));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    verticesRoofs.push_back(corner + glm::vec3(0, h, meter * 64));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    verticesRoofs.push_back(corner + glm::vec3(meter * 20, h, 0));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    verticesRoofs.push_back(corner + glm::vec3(meter * 20, h, meter * 44));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    verticesRoofs.push_back(corner + glm::vec3(0, h, meter * 64));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    verticesRoofs.push_back(corner + glm::vec3(meter * 20, h, meter * 44));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    verticesRoofs.push_back(corner + glm::vec3(meter * 64, h, meter * 64));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    verticesRoofs.push_back(corner + glm::vec3(meter * 64, h, meter * 44));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    indicesRoofs.push_back(indexRoofs);
    indicesRoofs.push_back(indexRoofs + 1);
    indicesRoofs.push_back(indexRoofs + 2);
    indicesRoofs.push_back(indexRoofs + 1);
    indicesRoofs.push_back(indexRoofs + 2);
    indicesRoofs.push_back(indexRoofs + 3);
    indexRoofs += 4;
    indicesRoofs.push_back(indexRoofs);
    indicesRoofs.push_back(indexRoofs + 1);
    indicesRoofs.push_back(indexRoofs + 2);
    indicesRoofs.push_back(indexRoofs + 1);
    indicesRoofs.push_back(indexRoofs + 2);
    indicesRoofs.push_back(indexRoofs + 3);
    indexRoofs += 4;

    verticesAP.push_back(corner);
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, -1));
    verticesAP.push_back(corner + glm::vec3(meter * 20, 0, 0));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, -1));
    verticesAP.push_back(corner + glm::vec3(0, h, 0));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, -1));
    verticesAP.push_back(corner + glm::vec3(meter * 20, h, 0));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, -1));
    indicesAP.push_back(indexAP);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 3);
    indexAP += 4;

    verticesAP.push_back(corner);
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(-1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(0, 0, meter * 64));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(-1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(0, h, 0));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(-1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(0, h, meter * 64));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(-1, 0, 0));
    indicesAP.push_back(indexAP);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 3);
    indexAP += 4;

    verticesAP.push_back(corner + glm::vec3(0, 0, meter * 64));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, 1));
    verticesAP.push_back(corner + glm::vec3(meter * 64, 0, meter * 64));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, 1));
    verticesAP.push_back(corner + glm::vec3(0, h, meter * 64));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, 1));
    verticesAP.push_back(corner + glm::vec3(meter * 64, h, meter * 64));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, 1));
    indicesAP.push_back(indexAP);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 3);
    indexAP += 4;

    verticesAP.push_back(corner + glm::vec3(meter * 64, 0, meter * 44));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(meter * 64, 0, meter * 64));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(meter * 64, h, meter * 44));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(meter * 64, h, meter * 64));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    indicesAP.push_back(indexAP);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 3);
    indexAP += 4;

    verticesAP.push_back(corner + glm::vec3(meter * 20, 0, meter * 44));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, -1));
    verticesAP.push_back(corner + glm::vec3(meter * 64, 0, meter * 44));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, -1));
    verticesAP.push_back(corner + glm::vec3(meter * 20, h, meter * 44));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, -1));
    verticesAP.push_back(corner + glm::vec3(meter * 64, h, meter * 44));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, -1));
    indicesAP.push_back(indexAP);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 3);
    indexAP += 4;

    verticesAP.push_back(corner + glm::vec3(meter * 44, 0, meter * 44));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(meter * 64, 0, meter * 64));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(meter * 44, h, meter * 44));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(meter * 64, h, meter * 64));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    indicesAP.push_back(indexAP);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 3);
    indexAP += 4;

    verticesAP.push_back(corner + glm::vec3(meter * 20, 0, 0));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(meter * 20, 0, meter * 44));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(meter * 20, h, 0));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(meter * 20, h, meter * 44));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    indicesAP.push_back(indexAP);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 3);
    indexAP += 4;
    return h;
}

void Tema3::CreateUApartments(glm::vec3 corner) {
    float h = CreateLApartments(corner);
    float X = corner.x;
    float H = corner.y;
    float Z = corner.z;
    float X2, Z2;
    int indicesToAdd = 12;
    if (indicesToAdd + indexAP > USHORT_MAX) {
        buildingsAP.back()->InitFromData(verticesAP, normalsAP, textCoordsAP, indicesAP);
        verticesAP.clear();
        normalsAP.clear();
        textCoordsAP.clear();
        indicesAP.clear();
        indexAP = 0;
        buildingsAP.push_back(new Mesh("buildings"));
    }
    float ap1w = meter * AP1W;
    float ap1h = meter * AP1W;

    verticesAP.push_back(corner + glm::vec3(meter * 44, 0 ,0));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(-1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(meter * 44, 0, meter * 44));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(-1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(meter * 44, h, 0));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(-1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(meter * 44, h, meter * 44));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(-1, 0, 0));
    indicesAP.push_back(indexAP);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 3);
    indexAP += 4;

    verticesAP.push_back(corner + glm::vec3(meter * 44, 0, 0));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, -1));
    verticesAP.push_back(corner + glm::vec3(meter * 64, 0, 0));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, -1));
    verticesAP.push_back(corner + glm::vec3(meter * 44, h, 0));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, -1));
    verticesAP.push_back(corner + glm::vec3(meter * 64, h, 0));
    X2 = -(maxAP / 50) * verticesAP.back().x + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(0, 0, -1));
    indicesAP.push_back(indexAP);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 3);
    indexAP += 4;

    verticesAP.push_back(corner + glm::vec3(meter * 64, 0, 0));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(meter * 64, 0, meter * 44));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(meter * 64, h, 0));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    verticesAP.push_back(corner + glm::vec3(meter * 64, h, meter * 44));
    X2 = -(maxAP / 50) * verticesAP.back().z + maxAP / 2;
    Z2 = (maxAP / 50) * (verticesAP.back().y - H) + maxAP / 2;
    textCoordsAP.push_back(glm::vec2(X2, Z2));
    normalsAP.push_back(glm::vec3(1, 0, 0));
    indicesAP.push_back(indexAP);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 1);
    indicesAP.push_back(indexAP + 2);
    indicesAP.push_back(indexAP + 3);
    indexAP += 4;

    if (indexRoofs + 4 >= USHORT_MAX) {
        roofs.back()->InitFromData(verticesRoofs, normalsRoofs, textCoordsRoofs, indicesRoofs);
        verticesRoofs.clear();
        normalsRoofs.clear();
        textCoordsRoofs.clear();
        indicesRoofs.clear();
        roofs.push_back(new Mesh("roofs"));
        indexRoofs = 0;
    }
    verticesRoofs.push_back(corner + glm::vec3(meter * 44, h, 0));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    verticesRoofs.push_back(corner + glm::vec3(meter * 44, h, meter * 44));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    verticesRoofs.push_back(corner + glm::vec3(meter * 64, h, 0));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    verticesRoofs.push_back(corner + glm::vec3(meter * 64, h, meter * 44));
    X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
    Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
    textCoordsRoofs.push_back(glm::vec2(X2, Z2));
    normalsRoofs.push_back(glm::vec3(0, 1, 0));
    indicesRoofs.push_back(indexRoofs);
    indicesRoofs.push_back(indexRoofs + 1);
    indicesRoofs.push_back(indexRoofs + 2);
    indicesRoofs.push_back(indexRoofs + 1);
    indicesRoofs.push_back(indexRoofs + 2);
    indicesRoofs.push_back(indexRoofs + 3);
    indexRoofs += 4;
}

void Tema3::CreateRandomBuilding(glm::vec3 corner, float dX, float dZ, float Hmin, float Hmax) {
    float btmax;
    int maxdivs;
    int texture = rand() % 4;
    float H = corner.y, X2, Z2;
    if (texture == 0) {
        btmax = maxMB1;
    } else if (texture == 1) {
        btmax = maxMB2;
    } else if (texture == 2) {
        btmax = maxOB1;
        Hmin /= 1.2;
        Hmax /= 1.2;
    } else {
        btmax = maxAP;
    }

    if (rand() % 1000 == 99 && texture != 2) {
        maxdivs = (rand() % 100 + 1000);
    } else {
        maxdivs = (rand() % (int)(Hmax - Hmin)) + Hmin;
    }

    int forms = rand() % 3 + 2;

    float dXp = dX;
    float dZp = dZ;
    dX = meter * dXp;
    dZ = meter * dZp;

    
    if (texture == 0) {             // modern building 1
        for (int i = 0; i < forms; i++) {
            int coef = rand() % 4 + 3;
            if (coef * 4 + indexMB1 > USHORT_MAX) {
                buildingsMB1.back()->InitFromData(verticesMB1, normalsMB1, textCoordsMB1, indicesMB1);
                verticesMB1.clear();
                normalsMB1.clear();
                textCoordsMB1.clear();
                indicesMB1.clear();
                indexMB1 = 0;
                buildingsMB1.push_back(new Mesh("buildings"));
            }
            float h = meter * (rand() % maxdivs + Hmin);
            glm::vec3 center = glm::vec3(meter * (rand() % ((int)(dXp / 3.0)) + dXp / 3.0), 0, meter * (rand() % ((int)(dZp / 3.0)) + dZp / 3.0));
            float radius = dX;
            if (dX - center.x < radius) {
                radius = dX - center.x;
            }
            if (dZ - center.y < radius) {
                radius = dZ - center.z;
            }
            if (center.x < radius) {
                radius = center.x;
            }
            if (center.z < radius) {
                radius = center.z;
            }
            center += corner;
            float angle = glm::radians(45.0);
            float delta = glm::radians(360.0 / coef);
            float halfdelta = delta / 2;
            for (int j = 0; j < coef; j++) {
                glm::vec3 normal = glm::vec3(sin(angle + halfdelta), 0, cos(angle + halfdelta));
                normalsMB1.push_back(normal);
                normalsMB1.push_back(normal);
                normalsMB1.push_back(normal);
                normalsMB1.push_back(normal);

                verticesMB1.push_back(center + glm::vec3(radius * sin(angle), 0, radius * cos(angle)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesMB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesMB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesMB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesMB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                }
                textCoordsMB1.push_back(glm::vec2(X2, Z2));

                verticesMB1.push_back(center + glm::vec3(radius * sin(angle + delta), 0, radius * cos(angle + delta)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesMB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesMB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesMB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesMB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                }
                textCoordsMB1.push_back(glm::vec2(X2, Z2));

                verticesMB1.push_back(center + glm::vec3(radius * sin(angle), h, radius * cos(angle)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesMB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesMB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesMB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesMB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                }
                textCoordsMB1.push_back(glm::vec2(X2, Z2));

                verticesMB1.push_back(center + glm::vec3(radius * sin(angle + delta), h, radius * cos(angle + delta)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesMB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesMB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesMB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesMB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB1.back().y - H) + btmax / 2;
                }
                textCoordsMB1.push_back(glm::vec2(X2, Z2));
                indicesMB1.push_back(indexMB1);
                indicesMB1.push_back(indexMB1 + 1);
                indicesMB1.push_back(indexMB1 + 2);
                indicesMB1.push_back(indexMB1 + 1);
                indicesMB1.push_back(indexMB1 + 2);
                indicesMB1.push_back(indexMB1 + 3);
                indexMB1 += 4;

                if (indexRoofs + 3 >= USHORT_MAX) {
                    roofs.back()->InitFromData(verticesRoofs, normalsRoofs, textCoordsRoofs, indicesRoofs);
                    verticesRoofs.clear();
                    normalsRoofs.clear();
                    textCoordsRoofs.clear();
                    indicesRoofs.clear();
                    roofs.push_back(new Mesh("roofs"));
                    indexRoofs = 0;
                }
                verticesRoofs.push_back(center + glm::vec3(0, h, 0));
                X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
                Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
                textCoordsRoofs.push_back(glm::vec2(X2, Z2));
                normalsRoofs.push_back(glm::vec3(0, 1, 0));
                verticesRoofs.push_back(center + glm::vec3(radius * sin(angle), h, radius * cos(angle)));
                X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
                Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
                textCoordsRoofs.push_back(glm::vec2(X2, Z2));
                normalsRoofs.push_back(glm::vec3(0, 1, 0));
                verticesRoofs.push_back(center + glm::vec3(radius * sin(angle + delta), h, radius * cos(angle + delta)));
                X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
                Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
                textCoordsRoofs.push_back(glm::vec2(X2, Z2));
                normalsRoofs.push_back(glm::vec3(0, 1, 0));
                indicesRoofs.push_back(indexRoofs);
                indicesRoofs.push_back(indexRoofs + 1);
                indicesRoofs.push_back(indexRoofs + 2);
                indexRoofs += 3;

                angle += delta;
            }
        }
    } else if (texture == 1) {      // modern building 2
        for (int i = 0; i < forms; i++) {
            int coef = rand() % 4 + 3;
            if (coef * 4 + indexMB2 > USHORT_MAX) {
                buildingsMB2.back()->InitFromData(verticesMB2, normalsMB2, textCoordsMB2, indicesMB2);
                verticesMB2.clear();
                normalsMB2.clear();
                textCoordsMB2.clear();
                indicesMB2.clear();
                indexMB2 = 0;
                buildingsMB2.push_back(new Mesh("buildings"));
            }
            float h = meter * (rand() % maxdivs + Hmin);
            glm::vec3 center = glm::vec3(meter * (rand() % ((int)(dXp / 3.0)) + dXp / 3.0), 0, meter * (rand() % ((int)(dZp / 3.0)) + dZp / 3.0));
            float radius = dX;
            if (dX - center.x < radius) {
                radius = dX - center.x;
            }
            if (dZ - center.y < radius) {
                radius = dZ - center.z;
            }
            if (center.x < radius) {
                radius = center.x;
            }
            if (center.z < radius) {
                radius = center.z;
            }
            center += corner;
            float angle = glm::radians(45.0);
            float delta = glm::radians(360.0 / coef);
            float halfdelta = delta / 2;
            for (int j = 0; j < coef; j++) {
                glm::vec3 normal = glm::vec3(sin(angle + halfdelta), 0, cos(angle + halfdelta));
                normalsMB2.push_back(normal);
                normalsMB2.push_back(normal);
                normalsMB2.push_back(normal);
                normalsMB2.push_back(normal);

                verticesMB2.push_back(center + glm::vec3(radius * sin(angle), 0, radius * cos(angle)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesMB2.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesMB2.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesMB2.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesMB2.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                }
                textCoordsMB2.push_back(glm::vec2(X2, Z2));

                verticesMB2.push_back(center + glm::vec3(radius * sin(angle + delta), 0, radius * cos(angle + delta)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesMB2.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesMB2.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesMB2.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesMB2.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                }
                textCoordsMB2.push_back(glm::vec2(X2, Z2));

                verticesMB2.push_back(center + glm::vec3(radius * sin(angle), h, radius * cos(angle)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesMB2.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesMB2.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesMB2.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesMB2.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                }
                textCoordsMB2.push_back(glm::vec2(X2, Z2));

                verticesMB2.push_back(center + glm::vec3(radius * sin(angle + delta), h, radius * cos(angle + delta)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesMB2.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesMB2.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesMB2.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesMB2.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesMB2.back().y - H) + btmax / 2;
                }
                textCoordsMB2.push_back(glm::vec2(X2, Z2));
                indicesMB2.push_back(indexMB2);
                indicesMB2.push_back(indexMB2 + 1);
                indicesMB2.push_back(indexMB2 + 2);
                indicesMB2.push_back(indexMB2 + 1);
                indicesMB2.push_back(indexMB2 + 2);
                indicesMB2.push_back(indexMB2 + 3);
                indexMB2 += 4;

                if (indexRoofs + 3 >= USHORT_MAX) {
                    roofs.back()->InitFromData(verticesRoofs, normalsRoofs, textCoordsRoofs, indicesRoofs);
                    verticesRoofs.clear();
                    normalsRoofs.clear();
                    textCoordsRoofs.clear();
                    indicesRoofs.clear();
                    roofs.push_back(new Mesh("roofs"));
                    indexRoofs = 0;
                }
                verticesRoofs.push_back(center + glm::vec3(0, h, 0));
                X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
                Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
                textCoordsRoofs.push_back(glm::vec2(X2, Z2));
                normalsRoofs.push_back(glm::vec3(0, 1, 0));
                verticesRoofs.push_back(center + glm::vec3(radius * sin(angle), h, radius * cos(angle)));
                X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
                Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
                textCoordsRoofs.push_back(glm::vec2(X2, Z2));
                normalsRoofs.push_back(glm::vec3(0, 1, 0));
                verticesRoofs.push_back(center + glm::vec3(radius * sin(angle + delta), h, radius * cos(angle + delta)));
                X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
                Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
                textCoordsRoofs.push_back(glm::vec2(X2, Z2));
                normalsRoofs.push_back(glm::vec3(0, 1, 0));
                indicesRoofs.push_back(indexRoofs);
                indicesRoofs.push_back(indexRoofs + 1);
                indicesRoofs.push_back(indexRoofs + 2);
                indexRoofs += 3;

                angle += delta;
            }
        }
    } else if (texture == 2) {      // old building
        for (int i = 0; i < forms; i++) {
            int coef = rand() % 4 + 3;
            if (coef * 4 + indexOB1 > USHORT_MAX) {
                buildingsOB1.back()->InitFromData(verticesOB1, normalsOB1, textCoordsOB1, indicesOB1);
                verticesOB1.clear();
                normalsOB1.clear();
                textCoordsOB1.clear();
                indicesOB1.clear();
                indexOB1 = 0;
                buildingsOB1.push_back(new Mesh("buildings"));
            }
            float h = meter * (rand() % maxdivs + Hmin);
            glm::vec3 center = glm::vec3(meter * (rand() % ((int)(dXp / 3.0)) + dXp / 3.0), 0, meter * (rand() % ((int)(dZp / 3.0)) + dZp / 3.0));
            float radius = dX;
            if (dX - center.x < radius) {
                radius = dX - center.x;
            }
            if (dZ - center.y < radius) {
                radius = dZ - center.z;
            }
            if (center.x < radius) {
                radius = center.x;
            }
            if (center.z < radius) {
                radius = center.z;
            }
            center += corner;
            float angle = glm::radians(45.0);
            float delta = glm::radians(360.0 / coef);
            float halfdelta = delta / 2;
            for (int j = 0; j < coef; j++) {
                glm::vec3 normal = glm::vec3(sin(angle + halfdelta), 0, cos(angle + halfdelta));
                normalsOB1.push_back(normal);
                normalsOB1.push_back(normal);
                normalsOB1.push_back(normal);
                normalsOB1.push_back(normal);

                verticesOB1.push_back(center + glm::vec3(radius * sin(angle), 0, radius * cos(angle)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesOB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesOB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesOB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesOB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                }
                textCoordsOB1.push_back(glm::vec2(X2, Z2));

                verticesOB1.push_back(center + glm::vec3(radius * sin(angle + delta), 0, radius * cos(angle + delta)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesOB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesOB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesOB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesOB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                }
                textCoordsOB1.push_back(glm::vec2(X2, Z2));

                verticesOB1.push_back(center + glm::vec3(radius * sin(angle), h, radius * cos(angle)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesOB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesOB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesOB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesOB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                }
                textCoordsOB1.push_back(glm::vec2(X2, Z2));

                verticesOB1.push_back(center + glm::vec3(radius * sin(angle + delta), h, radius * cos(angle + delta)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesOB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesOB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesOB1.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesOB1.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesOB1.back().y - H) + btmax / 2;
                }
                textCoordsOB1.push_back(glm::vec2(X2, Z2));
                indicesOB1.push_back(indexOB1);
                indicesOB1.push_back(indexOB1 + 1);
                indicesOB1.push_back(indexOB1 + 2);
                indicesOB1.push_back(indexOB1 + 1);
                indicesOB1.push_back(indexOB1 + 2);
                indicesOB1.push_back(indexOB1 + 3);
                indexOB1 += 4;

                if (indexRoofs + 3 >= USHORT_MAX) {
                    roofs.back()->InitFromData(verticesRoofs, normalsRoofs, textCoordsRoofs, indicesRoofs);
                    verticesRoofs.clear();
                    normalsRoofs.clear();
                    textCoordsRoofs.clear();
                    indicesRoofs.clear();
                    roofs.push_back(new Mesh("roofs"));
                    indexRoofs = 0;
                }
                verticesRoofs.push_back(center + glm::vec3(0, h, 0));
                X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
                Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
                textCoordsRoofs.push_back(glm::vec2(X2, Z2));
                normalsRoofs.push_back(glm::vec3(0, 1, 0));
                verticesRoofs.push_back(center + glm::vec3(radius * sin(angle), h, radius * cos(angle)));
                X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
                Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
                textCoordsRoofs.push_back(glm::vec2(X2, Z2));
                normalsRoofs.push_back(glm::vec3(0, 1, 0));
                verticesRoofs.push_back(center + glm::vec3(radius * sin(angle + delta), h, radius * cos(angle + delta)));
                X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
                Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
                textCoordsRoofs.push_back(glm::vec2(X2, Z2));
                normalsRoofs.push_back(glm::vec3(0, 1, 0));
                indicesRoofs.push_back(indexRoofs);
                indicesRoofs.push_back(indexRoofs + 1);
                indicesRoofs.push_back(indexRoofs + 2);
                indexRoofs += 3;

                angle += delta;
            }
        }
    } else if (texture == 3) {      // apartments
        for (int i = 0; i < forms; i++) {
            int coef = rand() % 4 + 3;
            if (coef * 4 + indexAP > USHORT_MAX) {
                buildingsAP.back()->InitFromData(verticesAP, normalsAP, textCoordsAP, indicesAP);
                verticesAP.clear();
                normalsAP.clear();
                textCoordsAP.clear();
                indicesAP.clear();
                indexAP = 0;
                buildingsAP.push_back(new Mesh("buildings"));
            }
            float h = meter * (rand() % maxdivs + Hmin);
            glm::vec3 center = glm::vec3(meter * (rand() % ((int)(dXp / 3.0)) + dXp / 3.0), 0, meter * (rand() % ((int)(dZp / 3.0)) + dZp / 3.0));
            float radius = dX;
            if (dX - center.x < radius) {
                radius = dX - center.x;
            }
            if (dZ - center.y < radius) {
                radius = dZ - center.z;
            }
            if (center.x < radius) {
                radius = center.x;
            }
            if (center.z < radius) {
                radius = center.z;
            }
            center += corner;
            float angle = glm::radians(45.0);
            float delta = glm::radians(360.0 / coef);
            float halfdelta = delta / 2;
            for (int j = 0; j < coef; j++) {
                glm::vec3 normal = glm::vec3(sin(angle + halfdelta), 0, cos(angle + halfdelta));
                normalsAP.push_back(normal);
                normalsAP.push_back(normal);
                normalsAP.push_back(normal);
                normalsAP.push_back(normal);

                verticesAP.push_back(center + glm::vec3(radius * sin(angle), 0, radius * cos(angle)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesAP.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesAP.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesAP.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesAP.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                }
                textCoordsAP.push_back(glm::vec2(X2, Z2));

                verticesAP.push_back(center + glm::vec3(radius * sin(angle + delta), 0, radius * cos(angle + delta)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesAP.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesAP.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesAP.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesAP.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                }
                textCoordsAP.push_back(glm::vec2(X2, Z2));

                verticesAP.push_back(center + glm::vec3(radius * sin(angle), h, radius * cos(angle)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesAP.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesAP.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesAP.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesAP.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                }
                textCoordsAP.push_back(glm::vec2(X2, Z2));

                verticesAP.push_back(center + glm::vec3(radius * sin(angle + delta), h, radius * cos(angle + delta)));
                if (angle < a1 || angle >= a4) {
                    X2 = -(btmax / 50) * verticesAP.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                } else if (angle >= a1 && angle < a2) {
                    X2 = -(btmax / 50) * verticesAP.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                } else if (angle >= a2 && angle < a3) {
                    X2 = -(btmax / 50) * verticesAP.back().x + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                } else {
                    X2 = -(btmax / 50) * verticesAP.back().z + btmax / 2;
                    Z2 = (btmax / 50) * (verticesAP.back().y - H) + btmax / 2;
                }
                textCoordsAP.push_back(glm::vec2(X2, Z2));
                indicesAP.push_back(indexAP);
                indicesAP.push_back(indexAP + 1);
                indicesAP.push_back(indexAP + 2);
                indicesAP.push_back(indexAP + 1);
                indicesAP.push_back(indexAP + 2);
                indicesAP.push_back(indexAP + 3);
                indexAP += 4;

                if (indexRoofs + 3 >= USHORT_MAX) {
                    roofs.back()->InitFromData(verticesRoofs, normalsRoofs, textCoordsRoofs, indicesRoofs);
                    verticesRoofs.clear();
                    normalsRoofs.clear();
                    textCoordsRoofs.clear();
                    indicesRoofs.clear();
                    roofs.push_back(new Mesh("roofs"));
                    indexRoofs = 0;
                }
                verticesRoofs.push_back(center + glm::vec3(0, h, 0));
                X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
                Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
                textCoordsRoofs.push_back(glm::vec2(X2, Z2));
                normalsRoofs.push_back(glm::vec3(0, 1, 0));
                verticesRoofs.push_back(center + glm::vec3(radius * sin(angle), h, radius * cos(angle)));
                X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
                Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
                textCoordsRoofs.push_back(glm::vec2(X2, Z2));
                normalsRoofs.push_back(glm::vec3(0, 1, 0));
                verticesRoofs.push_back(center + glm::vec3(radius * sin(angle + delta), h, radius * cos(angle + delta)));
                X2 = -((float)SCITYS / 50) * verticesRoofs.back().x + (float)SCITYS / 2;
                Z2 = ((float)SCITYS / 50) * verticesRoofs.back().z + (float)SCITYS / 2;
                textCoordsRoofs.push_back(glm::vec2(X2, Z2));
                normalsRoofs.push_back(glm::vec3(0, 1, 0));
                indicesRoofs.push_back(indexRoofs);
                indicesRoofs.push_back(indexRoofs + 1);
                indicesRoofs.push_back(indexRoofs + 2);
                indexRoofs += 3;

                angle += delta;
            }
        }
    }
}

void Tema3::CreateBuildingsSquares() { 
    float delta = 50.0 / CITYS;
    float X = 25 - 2.0 * delta;
    float Z = -25;
    float H = 0.2;
    bool done = false;
    int x = 0;
    for (int i = 0; i < CITYS - 1 && !done; i++) {
        for (int j = 0; j < CITYS - 1; j++) {
            if (!((matrix[i][j] == ROAD || matrix[i][j] == HIGHWAY)
                && (matrix[i][j + 1] == ROAD || matrix[i][j + 1] == HIGHWAY)
                && (matrix[i + 1][j] == ROAD || matrix[i + 1][j] == HIGHWAY)
                && (matrix[i + 1][j + 1] == ROAD || matrix[i + 1][j + 1] == HIGHWAY))) {
                Z += delta;
                continue;
            }
            int no_buildings = rand() % 4 + 1;
            glm::vec3 corner = glm::vec3(X + (streetLength + highwayWidth) / 2 + 2 * meter, H, Z + (streetLength + highwayWidth) / 2 + 2 * meter);
            if (no_buildings == 1) {
                int format = rand() % 50;
                if (format == 0) {              // two triangles
                    CreateTriangleBuilding(corner);
                } else if (format == 1) {       // two semicilinders
                    CreateCilinderBuilding(corner);
                } else if (format == 2) {       // L shaped building
                    CreateLApartments(corner);
                } else if (format == 3) {       // U shaped building
                    CreateUApartments(corner);
                } else if (format >= 4) {       // randomly generated builing
                    CreateRandomBuilding(corner, 64.0, 64.0, 80.0, 150.0);
                }
            } else if (no_buildings == 2) {
                int side = rand() % 2;
                if (side == 0) {                // left and right
                    CreateRandomBuilding(corner, 64, 30, 70, 200);
                    CreateRandomBuilding(corner + glm::vec3(0, 0, 34 * meter), 64, 30, 100, 200);
                } else if (side == 1) {         // up and down
                    CreateRandomBuilding(corner, 30, 64, 70, 200);
                    CreateRandomBuilding(corner + glm::vec3(34 * meter, 0, 0), 30, 64, 100, 200);
                }
            } else if (no_buildings == 3) {
                int side = rand() % 4;
                if (side == 0) {                // wide building up
                    CreateRandomBuilding(corner, 30, 30, 70, 200);
                    CreateRandomBuilding(corner + glm::vec3(0, 0, meter * 34), 30, 30, 70, 200);
                    CreateRandomBuilding(corner + glm::vec3(meter * 34, 0, 0), 30, 64, 100, 200);
                } else if (side == 1) {         // wide building right
                    CreateRandomBuilding(corner, 30, 30, 70, 200);
                    CreateRandomBuilding(corner + glm::vec3(meter * 34, 0, 0), 30, 30, 70, 200);
                    CreateRandomBuilding(corner + glm::vec3(0, 0, meter * 34), 64, 30, 100, 200);
                } else if (side == 2) {         // wide building down
                    CreateRandomBuilding(corner, 30, 64, 100, 200);
                    CreateRandomBuilding(corner + glm::vec3(meter * 34, 0, 0), 30, 30, 70, 200);
                    CreateRandomBuilding(corner + glm::vec3(meter * 34, 0, meter * 34), 30, 30, 70, 200);
                } else if (side == 3) {         // wide building left
                    CreateRandomBuilding(corner, 64, 30, 100, 200);
                    CreateRandomBuilding(corner + glm::vec3(meter * 34, 0, 0), 30, 30, 70, 200);
                    CreateRandomBuilding(corner + glm::vec3(0, 0, meter * 34), 30, 30, 70, 200);
                }
            } else if (no_buildings == 4) {
                CreateRandomBuilding(corner, 30, 30, 50, 150);
                CreateRandomBuilding(corner + glm::vec3(meter * 34, 0, 0), 30, 30, 50, 150);
                CreateRandomBuilding(corner + glm::vec3(0, 0, meter * 34), 30, 30, 50, 150);
                CreateRandomBuilding(corner + glm::vec3(meter * 34, 0, meter * 34), 30, 30, 50, 150);
            }
            Z += delta;
        }
        X -= delta;
        Z = -25;
    }
    endBuildingsSquares();
}

void Tema3::endBuildingsSquares() {
    if (indicesRoofs.size() != 0) {
        roofs.back()->InitFromData(verticesRoofs, normalsRoofs, textCoordsRoofs, indicesRoofs);
    } else {
        roofs.pop_back();
    }
    if (indicesMB1.size() != 0) {
        buildingsMB1.back()->InitFromData(verticesMB1, normalsMB1, textCoordsMB1, indicesMB1);
    } else {
        buildingsMB1.pop_back();
    }
    if (indicesAP.size() != 0) {
        buildingsAP.back()->InitFromData(verticesAP, normalsAP, textCoordsAP, indicesAP);
    } else {
        buildingsAP.pop_back();
    }
    if (indicesMB2.size() != 0) {
        buildingsMB2.back()->InitFromData(verticesMB2, normalsMB2, textCoordsMB2, indicesMB2);
    } else {
        buildingsMB2.pop_back();
    }
    if (indicesOB1.size() != 0) {
        buildingsOB1.back()->InitFromData(verticesOB1, normalsOB1, textCoordsOB1, indicesOB1);
    } else {
        buildingsOB1.pop_back();
    }
}

void Tema3::AddLampPost(float X, float Y, float Z) {
    float radius1 = 0.1 * meter;
    float radius2 = 0.07 * meter;
    float radius3 = 0.14 * meter;
    float h1 = 1 * meter;
    float h2 = 0.2 * meter;
    float h3 = 4 * meter;
    float h4 = 0.2 * meter;
    if (!true) {
        int x = 100;
        radius1 *= x;
        radius2 *= x;
        radius3 *= x;
        h1 *= x;
        h2 *= x;
        h3 *= x;
        h4 *= x;
    }
    float pi = 3.14;
    float delta = 2 * pi / CIRCLECORNERS;
    float angle = 0;
    glm::vec3 color = glm::vec3(1, 1, 1);
    float h = Y;
    if ((int)indexLP + 4 * CIRCLECORNERS + 8 > USHORT_MAX) {
        lampPosts.back()->InitFromData(verticesLP, indicesLP);
        lampPosts.push_back(new Mesh("lamps"));
        verticesLP.clear();
        indicesLP.clear();
        indexLP = 0;
    }
    for (int i = 0; i < CIRCLECORNERS; i++) {
        verticesLP.push_back(VertexFormat(glm::vec3(X, h, Z) + glm::vec3(cos(angle) * radius1, 0, sin(angle) * radius1), color));
        angle += delta;
    }
    angle = 0;
    h += h1;
    for (int i = 0; i < CIRCLECORNERS; i++) {
        verticesLP.push_back(VertexFormat(glm::vec3(X, h, Z) + glm::vec3(cos(angle) * radius1, 0, sin(angle) * radius1), color));
        angle += delta;
    }
    angle = 0;
    h += h2;
    for (int i = 0; i < CIRCLECORNERS; i++) {
        verticesLP.push_back(VertexFormat(glm::vec3(X, h, Z) + glm::vec3(cos(angle) * radius2, 0, sin(angle) * radius2), color));
        angle += delta;
    }
    h += h3;
    for (int i = 0; i < CIRCLECORNERS; i++) {
        verticesLP.push_back(VertexFormat(glm::vec3(X, h, Z) + glm::vec3(cos(angle) * radius2, 0, sin(angle) * radius2), color));
        angle += delta;
    }
    for (int i = 0; i < CIRCLECORNERS - 1; i++) {
        indicesLP.push_back(indexLP + i);
        indicesLP.push_back(indexLP + i + CIRCLECORNERS);
        indicesLP.push_back(indexLP + i + CIRCLECORNERS + 1);
        indicesLP.push_back(indexLP + i);
        indicesLP.push_back(indexLP + i + CIRCLECORNERS + 1);
        indicesLP.push_back(indexLP + i + 1);

        indicesLP.push_back(indexLP + i + CIRCLECORNERS);
        indicesLP.push_back(indexLP + i + CIRCLECORNERS * 2);
        indicesLP.push_back(indexLP + i + CIRCLECORNERS * 2 + 1);
        indicesLP.push_back(indexLP + i + CIRCLECORNERS);
        indicesLP.push_back(indexLP + i + CIRCLECORNERS * 2 + 1);
        indicesLP.push_back(indexLP + i + CIRCLECORNERS + 1);


        indicesLP.push_back(indexLP + i + CIRCLECORNERS * 2);
        indicesLP.push_back(indexLP + i + CIRCLECORNERS * 3);
        indicesLP.push_back(indexLP + i + CIRCLECORNERS * 3 + 1);
        indicesLP.push_back(indexLP + i + CIRCLECORNERS * 2);
        indicesLP.push_back(indexLP + i + CIRCLECORNERS * 3 + 1);
        indicesLP.push_back(indexLP + i + CIRCLECORNERS * 2 + 1);
    }
    indicesLP.push_back(indexLP + CIRCLECORNERS - 1);
    indicesLP.push_back(indexLP + CIRCLECORNERS * 2 - 1);
    indicesLP.push_back(indexLP + CIRCLECORNERS);
    indicesLP.push_back(indexLP + CIRCLECORNERS - 1);
    indicesLP.push_back(indexLP + CIRCLECORNERS);
    indicesLP.push_back(indexLP);

    indicesLP.push_back(indexLP + 2 * CIRCLECORNERS - 1);
    indicesLP.push_back(indexLP + CIRCLECORNERS * 3 - 1);
    indicesLP.push_back(indexLP + CIRCLECORNERS * 2);
    indicesLP.push_back(indexLP + CIRCLECORNERS * 2 - 1);
    indicesLP.push_back(indexLP + CIRCLECORNERS * 2);
    indicesLP.push_back(indexLP + CIRCLECORNERS);

    indicesLP.push_back(indexLP + 3 * CIRCLECORNERS - 1);
    indicesLP.push_back(indexLP + 4 * CIRCLECORNERS - 1);
    indicesLP.push_back(indexLP + 3 * CIRCLECORNERS);
    indicesLP.push_back(indexLP + CIRCLECORNERS * 3 - 1);
    indicesLP.push_back(indexLP + CIRCLECORNERS * 3);
    indicesLP.push_back(indexLP + CIRCLECORNERS * 2);
    indexLP += CIRCLECORNERS * 4;

    verticesLP.push_back(VertexFormat(glm::vec3(X - radius3, h, Z - radius3), color));
    verticesLP.push_back(VertexFormat(glm::vec3(X + radius3, h, Z - radius3), color));
    verticesLP.push_back(VertexFormat(glm::vec3(X + radius3, h, Z + radius3), color));
    verticesLP.push_back(VertexFormat(glm::vec3(X - radius3, h, Z + radius3), color));
    h += h4;
    verticesLP.push_back(VertexFormat(glm::vec3(X - radius3, h, Z - radius3), color));
    verticesLP.push_back(VertexFormat(glm::vec3(X + radius3, h, Z - radius3), color));
    verticesLP.push_back(VertexFormat(glm::vec3(X + radius3, h, Z + radius3), color));
    verticesLP.push_back(VertexFormat(glm::vec3(X - radius3, h, Z + radius3), color));

    indicesLP.push_back(indexLP);
    indicesLP.push_back(indexLP + 4);
    indicesLP.push_back(indexLP + 5);
    indicesLP.push_back(indexLP);
    indicesLP.push_back(indexLP + 1);
    indicesLP.push_back(indexLP + 5);

    indicesLP.push_back(indexLP + 1);
    indicesLP.push_back(indexLP + 5);
    indicesLP.push_back(indexLP + 6);
    indicesLP.push_back(indexLP + 1);
    indicesLP.push_back(indexLP + 2);
    indicesLP.push_back(indexLP + 6);

    indicesLP.push_back(indexLP + 2);
    indicesLP.push_back(indexLP + 6);
    indicesLP.push_back(indexLP + 7);
    indicesLP.push_back(indexLP + 2);
    indicesLP.push_back(indexLP + 3);
    indicesLP.push_back(indexLP + 7);

    indicesLP.push_back(indexLP + 3);
    indicesLP.push_back(indexLP + 7);
    indicesLP.push_back(indexLP + 4);
    indicesLP.push_back(indexLP + 3);
    indicesLP.push_back(indexLP + 4);
    indicesLP.push_back(indexLP);

    indexLP += 8;

    AddSpotlight(X, h, Z, 30, glm::vec3(1, 1, 1), glm::vec3(0, -1, 0));
}

void Tema3::endLampPost() {
    lampPosts.back()->InitFromData(verticesLP, indicesLP);
}

void Tema3::AddLampPosts() {
    float delta = 50.0 / CITYS;
    float X = 25 - delta;
    float Z = -25;
    float H = 0.2;
    float distCurb = 0.1 * meter;
    float m = (streetLength - highwayWidth) / 2;
    float p = (streetLength + highwayWidth) / 2;
    for (int i = 0; i < CITYS; i++) {
        for (int j = 0; j < CITYS; j++) {
            if (matrix[i][j] == ROAD
                || matrix[i][j] == HIGHWAY) {
                AddLampPost(X + m, H, Z + m);
                AddLampPost(X + m, H, Z + p);
                AddLampPost(X + p, H, Z + m);
                AddLampPost(X + p, H, Z + p);
            }
            Z += delta;
        }
        X -= delta;
        Z = -25;
    }
    endLampPost();
}

void Tema3::AddSpotlight(float X, float Y, float Z, float angle, glm::vec3 color, glm::vec3 direction) {
    light_props lp;
    lp.lightPosition = glm::vec3(X, Y, Z);
    lp.lightDirection = direction;
    lp.lightColor = color;
    lp.angle = angle;
    lp.spotlight = 1;

    lights.push_back(lp);
    int index = lights.size() - 1;
    if (settingsL == 3) {
        int x = floor(X);
        int z = floor(Z);
        int minX = x - 1, minZ = z - 1, maxX = x + 1, maxZ = z + 1;
        if (minX < -25) {
            minX = -25;
        }
        if (maxX > 24) {
            maxX = 24;
        }
        if (minZ < -25) {
            minZ = -25;
        }
        if (maxZ > 24) {
            maxZ = 24;
        }
        for (int i = minX; i <= maxX; i++) {
            for (int j = minZ; j <= maxZ; j++) {
                lightsToRender[i + 25][j + 25].push_back(index);
            }
        }
    } else if (settingsL == 2) {
        int x = floor(X);
        int z = floor(Z);
        int minX = x - 1, minZ = z - 1, maxX = x + 1, maxZ = z + 1;
        if (minX < -25) {
            minX = -25;
        }
        if (maxX > 24) {
            maxX = 24;
        }
        if (minZ < -25) {
            minZ = -25;
        }
        if (maxZ > 24) {
            maxZ = 24;
        }
        lightsToRender[x + 25][z + 25].push_back(index);
        if (minX < x) {
            lightsToRender[minX + 25][z + 25].push_back(index);
        }
        if (maxX > x) {
            lightsToRender[maxX + 25][z + 25].push_back(index);
        }
        if (minZ < z) {
            lightsToRender[x + 25][minZ + 25].push_back(index);
        }
        if (maxZ > z) {
            lightsToRender[x + 25][maxZ + 25].push_back(index);
        }
    } else if (settingsL == 1) {
        int x = floor(X);
        int z = floor(Z);
        lightsToRender[x + 25][z + 25].push_back(index);
    }
}

Tema3::Tema3() {

}

Tema3::~Tema3()
{
}

void Tema3::Init() {
	{
		Shader *shader = new Shader("ShaderColor");
		shader->AddShader("Source/Teme/Tema3/Shaders/VertexShaderColor.glsl", GL_VERTEX_SHADER);
		shader->AddShader("Source/Teme/Tema3/Shaders/FragmentShaderColor.glsl", GL_FRAGMENT_SHADER);
		shader->CreateAndLink();
		shaders[shader->GetName()] = shader;
	}

    {
        Shader *shader = new Shader("ShaderTexture");
        shader->AddShader("Source/Teme/Tema3/Shaders/VertexShaderTexture.glsl", GL_VERTEX_SHADER);
        shader->AddShader("Source/Teme/Tema3/Shaders/FragmentShaderTexture.glsl", GL_FRAGMENT_SHADER);
        shader->CreateAndLink();
        shaders[shader->GetName()] = shader;
    } 

    const std::string textureLoc = "Source/Teme/Tema3/Textures/";

    {
        Texture2D* texture = new Texture2D();
        texture->Load2D((textureLoc + "grass.png").c_str(), GL_REPEAT);
        mapTextures["grass"] = texture;
    }

    {
        Texture2D* texture = new Texture2D();
        texture->Load2D((textureLoc + "skybox.png").c_str(), GL_REPEAT);
        mapTextures["skybox"] = texture;
    }
    
    {
        Texture2D* texture = new Texture2D();
        texture->Load2D((textureLoc + "pavement.png").c_str(), GL_REPEAT);
        mapTextures["pavement"] = texture;
    }

    {
        Texture2D* texture = new Texture2D();
        texture->Load2D((textureLoc + "sand.png").c_str(), GL_REPEAT);
        mapTextures["land"] = texture;
    }

    {
        Texture2D* texture = new Texture2D();
        texture->Load2D((textureLoc + "asphalt.png").c_str(), GL_REPEAT);
        mapTextures["asphalt"] = texture;
    }

    {
        Texture2D* texture = new Texture2D();
        texture->Load2D((textureLoc + "concrete.png").c_str(), GL_REPEAT);
        mapTextures["concrete"] = texture;
    }

    {
        Texture2D* texture = new Texture2D();
        texture->Load2D((textureLoc + "apartments1.png").c_str(), GL_REPEAT);
        mapTextures["AP"] = texture;
    }

    {
        Texture2D* texture = new Texture2D();
        texture->Load2D((textureLoc + "concrete2.png").c_str(), GL_REPEAT);
        mapTextures["concrete2"] = texture;
    }

    {
        Texture2D* texture = new Texture2D();
        texture->Load2D((textureLoc + "modern-building1.png").c_str(), GL_REPEAT);
        mapTextures["MB1"] = texture;
    }

    {
        Texture2D* texture = new Texture2D();
        texture->Load2D((textureLoc + "modern-building2.png").c_str(), GL_REPEAT);
        mapTextures["MB2"] = texture;
    }

    {
        Texture2D* texture = new Texture2D();
        texture->Load2D((textureLoc + "old-building.png").c_str(), GL_REPEAT);
        mapTextures["OB1"] = texture;
    }

    {
        Mesh* mesh = new Mesh("sphere");
        mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "sphere.obj");
        meshes[mesh->GetMeshID()] = mesh;
    }

    {
        Mesh* mesh = new Mesh("tree");
        mesh->LoadMesh(textureLoc, "tree.obj");
        meshes[mesh->GetMeshID()] = mesh;
    }

    {
        land = new Mesh("land");
        std::vector<glm::vec3> vertices = {
            glm::vec3(-25, 0, -25),
            glm::vec3(25, 0, -25),
            glm::vec3(25, 0, 25),
            glm::vec3(-25, 0, 25),
            glm::vec3(0, 0, 0)
        };

        std::vector<unsigned short> indices = {
            0, 4, 1,
            1, 4, 2,
            2, 4, 3,
            3, 4, 0
        };

        std::vector<glm::vec3> normals{
            glm::vec3(0, 1, 0),
            glm::vec3(0, 1, 0),
            glm::vec3(0, 1, 0),
            glm::vec3(0, 1, 0),
            glm::vec3(0, 1, 0)
        };

        std::vector<glm::vec2> textureCoords {
            glm::vec2(0, 0),
            glm::vec2(SCITYS, 0),
            glm::vec2(SCITYS, SCITYS),
            glm::vec2(0, SCITYS),
            glm::vec2((float)SCITYS / 2, (float)SCITYS / 2)
        };

        land->InitFromData(vertices, normals, textureCoords, indices);
        landMatrix = glm::translate(glm::mat4(1), glm::vec3(0, 0.15, 0));
    }

	//Light & material properties
    {
        light_props lp;
        lp.lightPosition = glm::vec3(60, 10, 0);
        lp.lightDirection = glm::vec3(0, 0, -1);
        lp.angle = 0;
        lp.spotlight = 0;
        lights.push_back(lp);
    }

    materialShininess = 1;
    materialKd = 0.1;
    materialKs = 0.5;

    default.shininess = 14;
    default.kd = 0.1;
    default.ks = 0.5;

    ground.shininess = 15;
    ground.kd = 0.1;
    ground.ks = 0.5;

    building.shininess = 11;
    building.kd = 0.02;
    building.ks = 0.06;

    GeneratePlane();
    CreateBuildingsSideWalks();
    AddLampPosts();
    int max = 0;
    for (int i = 0; i < 50; i++) {
        for (int j = 0; j < 50; j++) {
            if (max < lightsToRender[i][j].size()) {
                max = lightsToRender[i][j].size();
            }
        }
    }
    std::cout << "lights set to: ";
    if (settingsL == 0) {
        std::cout << "ultra low\n";
    } else if (settingsL == 1) {
        std::cout << "low\n";
    } else if (settingsL == 2) {
        std::cout << "normal\n";
    } else if (settingsL == 3) {
        std::cout << "high\n";
    }
    std::cout << "max number of light poles in one chunk " << max << std::endl;
    CreateBuildingsSquares();
    int n = 5;
    n += strs.mesh.size();
    n += pavements.size();
    n += lampPosts.size();
    n += lampPosts.size();
    n += buildingsMB1.size();
    n += buildingsMB2.size();
    n += buildingsOB1.size();
    n += buildingsAP.size();
    n += roofs.size();
    std::cout << "total number of meshes generated: " << n << "\n";
    double m = USHORT_MAX;
    m += (strs.mesh.size() - 1) * USHORT_MAX;
    m += (pavements.size() - 1) * USHORT_MAX;
    m += (lampPosts.size() - 1) * USHORT_MAX;
    m += (lampPosts.size() - 1) * USHORT_MAX;
    m += (buildingsMB1.size() - 1) * USHORT_MAX;
    m += (buildingsMB2.size() - 1) * USHORT_MAX;
    m += (buildingsOB1.size() - 1) * USHORT_MAX;
    m += (buildingsAP.size() - 1) * USHORT_MAX;
    m += (roofs.size() - 1) * USHORT_MAX;
    std::cout << "approximate number of indices: " << m << "\n";
}

void Tema3::FrameStart() {
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	glViewport(0, 0, resolution.x, resolution.y);
}

void Tema3::Update(float deltaTimeSeconds) {
    Render();
}

void Tema3::Render() {
    {
        glm::mat4 modelMatrix = glm::mat4(1);
        modelMatrix = glm::translate(modelMatrix, glm::vec3(0, 0, 0));
        modelMatrix = glm::scale(modelMatrix, glm::vec3(200));
        modelMatrix = glm::rotate(modelMatrix, (float)glm::radians(45.0), glm::vec3(0, 1, 0));
        RenderTextureMesh(meshes["sphere"], modelMatrix, mapTextures["skybox"], &default);
    }

    RenderTextureMesh(land, landMatrix, mapTextures["land"], &ground);
    for (auto &mesh : strs.mesh) {
        RenderTextureMesh(mesh, strs.modelMatrix, mapTextures["asphalt"], &ground);
    }
    RenderTextureMesh(parkComplex.mesh, parkComplex.modelMatrix, mapTextures["grass"], &ground);
    for (auto &it : pavements) {
        RenderTextureMesh(it, glm::mat4(1), mapTextures["pavement"], &ground);
    }
    RenderColorMesh(waterComplex.mesh, waterComplex.modelMatrix, &default, glm::vec3(0.11, 0.56, 1));
    RenderTextureMesh(compoundComplex.mesh, compoundComplex.modelMatrix, mapTextures["concrete"], &ground);
    for (auto &it : lampPosts) {
        RenderColorMesh(it, glm::mat4(1), &default, glm::vec3(1, 1, 1));
    }
    if (buildingsMB1.size() != 0) {
        for (auto &it : buildingsMB1) {
            RenderTextureMesh(it, glm::mat4(1), mapTextures["MB1"], &building);
        }
    }
    if (buildingsAP.size() != 0) {
        for (auto &it : buildingsAP) {
            RenderTextureMesh(it, glm::mat4(1), mapTextures["AP"], &building);
        }
    }
    if (buildingsOB1.size() != 0) {
        for (auto &it : buildingsOB1) {
            RenderTextureMesh(it, glm::mat4(1), mapTextures["OB1"], &building);
        }
    }
    if (buildingsMB2.size() != 0) {
        for (auto &it : buildingsMB2) {
            RenderTextureMesh(it, glm::mat4(1), mapTextures["MB2"], &building);
        }
    }
    if (indicesRoofs.size() != 0) {
        for (auto &it : roofs) {
            RenderTextureMesh(it, glm::mat4(1), mapTextures["concrete2"], &building);
        }
    }
}

void Tema3::FrameEnd() {
	DrawCoordinatSystem();
}

void Tema3::RenderTextureMesh(Mesh *mesh, const glm::mat4 & modelMatrix, Texture2D* texture, material_props* mat) {
    Shader* shader = shaders["ShaderTexture"];
    if (!mesh || !shader || !shader->GetProgramID())
        return;

    glm::vec3 eyePosition = GetSceneCamera()->transform->GetWorldPosition();
    glUseProgram(shader->program);

    int x = floor(eyePosition.x) + 25;
    int z = floor(eyePosition.z) + 25;
    bool todo = true;
    int max_lights = 1;
    if (x < 0 || z < 0 || x > 50 || z > 50) {
        todo = false;
    }
    if (todo) {
        max_lights += lightsToRender[x][z].size();
    }

    GLint loc_max_lights = glGetUniformLocation(shader->program, "max_lights");
    glUniform1i(loc_max_lights, max_lights);

    {
        int i = 0;
        char buffer[64];
        sprintf(buffer, "lights[%d].light_position", i);
        int light_position = glGetUniformLocation(shader->program, buffer);
        glUniform3f(light_position, lights.at(i).lightPosition.x, lights.at(i).lightPosition.y, lights.at(i).lightPosition.z);

        sprintf(buffer, "lights[%d].light_direction", i);
        int light_direction = glGetUniformLocation(shader->program, buffer);
        glUniform3f(light_direction, lights.at(i).lightDirection.x, lights.at(i).lightDirection.y, lights.at(i).lightDirection.z);

        sprintf(buffer, "lights[%d].Spotlight", i);
        int spotlight_loc = glGetUniformLocation(shader->program, buffer);
        glUniform1i(spotlight_loc, lights.at(i).spotlight);

        sprintf(buffer, "lights[%d].Angle", i);
        int angle_loc = glGetUniformLocation(shader->program, buffer);
        glUniform1f(angle_loc, lights.at(i).angle);
        
        sprintf(buffer, "lights[%d].light_color", i);
        int light_color_loc = glGetUniformLocation(shader->program, buffer);
        glUniform3f(light_color_loc, lights.at(i).lightColor.x, lights.at(i).lightColor.y, lights.at(i).lightColor.z);
    }

    if (todo) {
        int d = 1;
        for (int j = 0; j < lightsToRender[x][z].size() && d < 299; j++) {
            int i = lightsToRender[x][z].at(j);
            char buffer[64];
            sprintf(buffer, "lights[%d].light_position", d);
            int light_position = glGetUniformLocation(shader->program, buffer);
            glUniform3f(light_position, lights.at(i).lightPosition.x, lights.at(i).lightPosition.y, lights.at(i).lightPosition.z);

            sprintf(buffer, "lights[%d].light_direction", d);
            int light_direction = glGetUniformLocation(shader->program, buffer);
            glUniform3f(light_direction, lights.at(i).lightDirection.x, lights.at(i).lightDirection.y, lights.at(i).lightDirection.z);

            sprintf(buffer, "lights[%d].Spotlight", d);
            int spotlight_loc = glGetUniformLocation(shader->program, buffer);
            glUniform1i(spotlight_loc, lights.at(i).spotlight);

            sprintf(buffer, "lights[%d].Angle", d);
            int angle_loc = glGetUniformLocation(shader->program, buffer);
            glUniform1f(angle_loc, lights.at(i).angle);

            sprintf(buffer, "lights[%d].light_color", d);
            int light_color_loc = glGetUniformLocation(shader->program, buffer);
            glUniform3f(light_color_loc, lights.at(i).lightColor.x, lights.at(i).lightColor.y, lights.at(i).lightColor.z);
            d++;
        }
    }

    GLint loc_model_matrix = glGetUniformLocation(shader->program, "Model");
    glUniformMatrix4fv(loc_model_matrix, 1, GL_FALSE, glm::value_ptr(modelMatrix));

    glm::mat4 viewMatrix = GetSceneCamera()->GetViewMatrix();
    int loc_view_matrix = glGetUniformLocation(shader->program, "View");
    glUniformMatrix4fv(loc_view_matrix, 1, GL_FALSE, glm::value_ptr(viewMatrix));

    glm::mat4 projectionMatrix = GetSceneCamera()->GetProjectionMatrix();
    int loc_projection_matrix = glGetUniformLocation(shader->program, "Projection");
    glUniformMatrix4fv(loc_projection_matrix, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

    if (texture) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture->GetTextureID());
        glUniform1i(glGetUniformLocation(shader->program, "texture"), 0);
    }

    int eye_position = glGetUniformLocation(shader->program, "eye_position");
    glUniform3f(eye_position, eyePosition.x, eyePosition.y, eyePosition.z);

    int material_shininess = glGetUniformLocation(shader->program, "material_shininess");
    glUniform1i(material_shininess, mat->shininess);

    int material_kd = glGetUniformLocation(shader->program, "material_kd");
    glUniform1f(material_kd, mat->kd);

    int material_ks = glGetUniformLocation(shader->program, "material_ks");
    glUniform1f(material_ks, mat->ks);

    glBindVertexArray(mesh->GetBuffers()->VAO);
    glDrawElements(mesh->GetDrawMode(), static_cast<int>(mesh->indices.size()), GL_UNSIGNED_SHORT, 0);
}

void Tema3::RenderColorMesh(Mesh *mesh,  const glm::mat4 & modelMatrix, material_props* mat, const glm::vec3 &color) {
    Shader* shader = shaders["ShaderColor"];
    if (!mesh || !shader || !shader->GetProgramID())
        return;

    glUseProgram(shader->program);
    glm::vec3 eyePosition = GetSceneCamera()->transform->GetWorldPosition();
    int x = floor(eyePosition.x) + 25;
    int z = floor(eyePosition.z) + 25;
    bool todo = true;
    int max_lights = 1;
    if (x < 0 || z < 0 || x > 50 || z > 50) {
        todo = false;
    }
    if (todo) {
        max_lights += lightsToRender[x][z].size();
    }


    GLint loc_max_lights = glGetUniformLocation(shader->program, "max_lights");
    glUniform1i(loc_max_lights, max_lights);

    for (int i = 0; i < 1; i++) {
        char buffer[64];
        sprintf(buffer, "lights[%d].light_position", i);
        int light_position = glGetUniformLocation(shader->program, buffer);
        glUniform3f(light_position, lights.at(i).lightPosition.x, lights.at(i).lightPosition.y, lights.at(i).lightPosition.z);

        sprintf(buffer, "lights[%d].light_direction", i);
        int light_direction = glGetUniformLocation(shader->program, buffer);
        glUniform3f(light_direction, lights.at(i).lightDirection.x, lights.at(i).lightDirection.y, lights.at(i).lightDirection.z);

        sprintf(buffer, "lights[%d].Spotlight", i);
        int spotlight_loc = glGetUniformLocation(shader->program, buffer);
        glUniform1i(spotlight_loc, lights.at(i).spotlight);

        sprintf(buffer, "lights[%d].Angle", i);
        int angle_loc = glGetUniformLocation(shader->program, buffer);
        glUniform1f(angle_loc, lights.at(i).angle);

        sprintf(buffer, "lights[%d].light_color", i);
        int light_color_loc = glGetUniformLocation(shader->program, buffer);
        glUniform3f(light_color_loc, lights.at(i).lightColor.x, lights.at(i).lightColor.y, lights.at(i).lightColor.z);
    }

    if (todo) {
        int d = 1;
        for (int j = 0; j < lightsToRender[x][z].size() && d < 299; j++) {
            int i = lightsToRender[x][z].at(j);
            char buffer[64];
            sprintf(buffer, "lights[%d].light_position", d);
            int light_position = glGetUniformLocation(shader->program, buffer);
            glUniform3f(light_position, lights.at(i).lightPosition.x, lights.at(i).lightPosition.y, lights.at(i).lightPosition.z);

            sprintf(buffer, "lights[%d].light_direction", d);
            int light_direction = glGetUniformLocation(shader->program, buffer);
            glUniform3f(light_direction, lights.at(i).lightDirection.x, lights.at(i).lightDirection.y, lights.at(i).lightDirection.z);

            sprintf(buffer, "lights[%d].Spotlight", d);
            int spotlight_loc = glGetUniformLocation(shader->program, buffer);
            glUniform1i(spotlight_loc, lights.at(i).spotlight);

            sprintf(buffer, "lights[%d].Angle", d);
            int angle_loc = glGetUniformLocation(shader->program, buffer);
            glUniform1f(angle_loc, lights.at(i).angle);

            sprintf(buffer, "lights[%d].light_color", d);
            int light_color_loc = glGetUniformLocation(shader->program, buffer);
            glUniform3f(light_color_loc, lights.at(i).lightColor.x, lights.at(i).lightColor.y, lights.at(i).lightColor.z);
            d++;
        }
    }

    int eye_position = glGetUniformLocation(shader->program, "eye_position");
    glUniform3f(eye_position, eyePosition.x, eyePosition.y, eyePosition.z);

    int material_shininess = glGetUniformLocation(shader->program, "material_shininess");
    glUniform1i(material_shininess, mat->shininess);

    int material_kd = glGetUniformLocation(shader->program, "material_kd");
    glUniform1f(material_kd, mat->kd);

    int material_ks = glGetUniformLocation(shader->program, "material_ks");
    glUniform1f(material_ks, mat->ks);

    int object_color = glGetUniformLocation(shader->program, "object_color");
    glUniform3f(object_color, color.r, color.g, color.b);

    GLint loc_model_matrix = glGetUniformLocation(shader->program, "Model");
    glUniformMatrix4fv(loc_model_matrix, 1, GL_FALSE, glm::value_ptr(modelMatrix));

    glm::mat4 viewMatrix = GetSceneCamera()->GetViewMatrix();
    int loc_view_matrix = glGetUniformLocation(shader->program, "View");
    glUniformMatrix4fv(loc_view_matrix, 1, GL_FALSE, glm::value_ptr(viewMatrix));

    glm::mat4 projectionMatrix = GetSceneCamera()->GetProjectionMatrix();
    int loc_projection_matrix = glGetUniformLocation(shader->program, "Projection");
    glUniformMatrix4fv(loc_projection_matrix, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

    glBindVertexArray(mesh->GetBuffers()->VAO);
    glDrawElements(mesh->GetDrawMode(), static_cast<int>(mesh->indices.size()), GL_UNSIGNED_SHORT, 0);
}

void Tema3::OnInputUpdate(float deltaTime, int mods) {

}

void Tema3::OnKeyPress(int key, int mods) {

}

void Tema3::OnKeyRelease(int key, int mods) {

}

void Tema3::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) {

}

void Tema3::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) {

}

void Tema3::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) {
	
}

void Tema3::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) {

}

void Tema3::OnWindowResize(int width, int height) {

}
