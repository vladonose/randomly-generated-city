#pragma once
#include <Component/SimpleScene.h>
#include <Component/Transform/Transform.h>
#include <Core/GPU/Mesh.h>
#include <Core/Engine.h>
#include "Street.h"
#include "Multiples.h"
#include <vector>
#include <string>
#include <iostream>
#include <math.h>

class Tema3 : public SimpleScene
{
	public:
		Tema3();
		~Tema3();

		void Init() override;

	private:
		void FrameStart() override;
		void Update(float deltaTimeSeconds) override;
		void FrameEnd() override;

		void RenderTextureMesh(Mesh *mesh, const glm::mat4 &modelMatrix, Texture2D* texture, material_props* mat); 
        void RenderColorMesh(Mesh *mesh, const glm::mat4 &modelMatrix, material_props* mat, const glm::vec3 &color = glm::vec3(1));

		void OnInputUpdate(float deltaTime, int mods) override;
		void OnKeyPress(int key, int mods) override;
		void OnKeyRelease(int key, int mods) override;
		void OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) override;
		void OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) override;
		void OnWindowResize(int width, int height) override;

        void Render();
        void AddRoads(int line, int col);
        void AddHighway(int line, int col, direction genDirection, direction currDirection);
        void PrintASCIIMap();
        void GeneratePlane();
        void Discover(undiscovered* land, int line, int col, int type);
        void CreateLand(int line, int col, int type);
        void CreateComplexMeshes();
        void CreateStreetMeshes();
        void CreateBuildingsSideWalks();
        void CreateBuildingsSquares();
        void AddLampPost(float X, float Y, float Z);
        void endLampPost();
        void AddLampPosts();
        void AddSpotlight(float X, float Y, float Z, float angle, glm::vec3 color, glm::vec3 direction);
        void CreateTriangleBuilding(glm::vec3 corner);
        void CreateCilinderBuilding(glm::vec3 corner);
        float CreateLApartments(glm::vec3 corner);
        void CreateUApartments(glm::vec3 corner);
        void CreateRandomBuilding(glm::vec3 corner, float dX, float dZ, float Hmin, float Hmax);
        void endBuildingsSquares();

        std::vector<Mesh*> pavements = { new Mesh("pavements") };
        std::vector<Mesh*> lampPosts = { new Mesh("lamps") };
        std::vector<VertexFormat> verticesLP;
        std::vector<unsigned short> indicesLP;
        int indexLP = 0;

        material_props default;
        material_props building;
        material_props ground;

        bool mb1r = false;
        bool mb2r = false;
        bool ob1r = false;
        bool apr = false;
        std::vector<glm::vec3> verticesAlt;
        std::vector<glm::vec3> normalsAlt;
        std::vector<glm::vec2> textCoordsAlt;
        std::vector<unsigned short> indicesAlt;
        int indexAlt = 0;

        std::vector<Mesh*> buildingsMB1 = { new Mesh("buildings") };
        std::vector<glm::vec3> verticesMB1;
        std::vector<glm::vec3> normalsMB1;
        std::vector<glm::vec2> textCoordsMB1;
        std::vector<unsigned short> indicesMB1;
        int indexMB1 = 0;
        float maxMB1 = 600;

        std::vector<Mesh*> buildingsMB2 = { new Mesh("buildings") };
        std::vector<glm::vec3> verticesMB2;
        std::vector<glm::vec3> normalsMB2;
        std::vector<glm::vec2> textCoordsMB2;
        std::vector<unsigned short> indicesMB2;
        int indexMB2 = 0;
        float maxMB2 = 600;

        std::vector<Mesh*> buildingsOB1 = { new Mesh("buildings") };
        std::vector<glm::vec3> verticesOB1;
        std::vector<glm::vec3> normalsOB1;
        std::vector<glm::vec2> textCoordsOB1;
        std::vector<unsigned short> indicesOB1;
        int indexOB1 = 0;
        float maxOB1 = 600;

        std::vector<Mesh*> buildingsAP = { new Mesh("buildings") };
        std::vector<glm::vec3> verticesAP;
        std::vector<glm::vec3> normalsAP;
        std::vector<glm::vec2> textCoordsAP;
        std::vector<unsigned short> indicesAP;
        int indexAP = 0;
        float maxAP = 300;

        std::vector<Mesh *> roofs = { new Mesh("roofs") };
        std::vector<glm::vec3> verticesRoofs;
        std::vector<glm::vec3> normalsRoofs;
        std::vector<glm::vec2> textCoordsRoofs;
        std::vector<unsigned short> indicesRoofs;
        int indexRoofs = 0;

        Mesh* land;
        glm::mat4 landMatrix;
        Complex waterComplex = Complex(CITYS + 1);
        Complex parkComplex = Complex(CITYS + 1);
        Complex compoundComplex = Complex(CITYS + 1);

        int settingsL = 1;          // CHANGE THIS TO MODIFY THE LIGHTS SETTINGS
                                    // 0 - no lamp post lights
                                    // 1 - 16 lights per chunk
                                    // 2 - 80 lights per chunk
                                    // 3 - 144 lights per chunk - laggy af

        Streets strs = Streets();
        std::vector<light_props> lights;
        std::vector<int> lightsToRender[50][50];
        int indexT = 0;
		unsigned int materialShininess;
		float materialKd;
		float materialKs;
        float dAngle = 2;
        int maxCS = CITYS * CITYS / 2;
        int capacityCS = maxCS;
        roadCell roadMap[CITYS][CITYS];
        int matrix[CITYS][CITYS];   // 1 - 2 way road
                                    // 2 - 4 way road
                                    // 3 - land
                                    // 4 - water
                                    // 5 - park
                                    // 6 - big building
                                    // intermediary: 0 - the nothingness at the beginning
                                    //              -1 - land discovery
        float a1 = glm::radians(45.0);
        float a2 = glm::radians(135.0);
        float a3 = glm::radians(225.0);
        float a4 = glm::radians(315.0);

        int bsmatrix[CITYS - 1][CITYS - 1]; 
        float highwayDivThreshold = 98.8;
        std::list<roadGenerator> divergents;
        float meter = 5.0 / (8.0 * CITYS);
        float roadWidth = 6 * meter;
        float highwayWidth = 12 * meter;
        float streetLength = 80 * meter;
        std::unordered_map<std::string, Texture2D*> mapTextures;
};
