#pragma once
#include <iostream>
#include <Core/Engine.h>
#include "Multiples.h"

typedef enum {
    UP, DOWN, LEFT, RIGHT, CENTER
}direction;

class Streets {
public:
    std::string name;
    std::vector<Mesh*> mesh = { new Mesh("streets") };
    glm::mat4 modelMatrix = glm::mat4(1);
    std::vector<glm::vec3> vertices;
    std::vector<unsigned short> indices;
    std::vector<glm::vec2> textureCoords;
    glm::vec3 color = glm::vec3(0.5, 0.5, 0.5);
    std::vector<glm::vec3> normals;
    int index = 0;

    Streets();

    ~Streets();

    void CreateMeshStreet(direction side, glm::vec3 ldcorner, float length, float width, float width2);

    void CreateSimpleStreet(direction side, glm::vec3 ldcorner, float length, float width, float width2);

    void end(std::vector<glm::vec2> textureCoords, std::vector<glm::vec3> normals);
};