#include "Multiples.h"

Complex::Complex(int size) {
    this->size = size;
    matrix = new cell*[size];
    for (int i = 0; i < size; i++) {
        matrix[i] = new cell[size];
    }
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            matrix[i][j].down = false;
            matrix[i][j].up = false;
            matrix[i][j].right = false;
            matrix[i][j].left = false;
            matrix[i][j].value = -1;
        }
    }
    mesh = new Mesh("complex");
    modelMatrix = glm::mat4(1);
}

Complex::Complex() {

}

Complex::~Complex() {
    for (int i = 0; i < size; i++) {
        delete[] matrix[i];
    }
    delete[] matrix;
}

