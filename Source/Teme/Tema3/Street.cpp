#include <string>
#include <Core/Engine.h>
#include <iostream>
#include "Street.h"

Streets::Streets() {

}

Streets::~Streets() {

}

void Streets::CreateMeshStreet(direction side, glm::vec3 ldcorner, float length, float width, float width2) {
    if (index + 4 > USHORT_MAX) {
        mesh.back()->InitFromData(vertices, normals, textureCoords, indices);
        mesh.push_back(new Mesh("streets"));
        index = 0;
        vertices = {};
        textureCoords = {};
        normals = {};
    }
    int zero = index;
    int one = zero + 1;
    int two = one + 1;
    int three = two + 1;
    index = index + 4;
    if (side == UP) {
        glm::vec3 vf = ldcorner + glm::vec3((length + width) / 2, 0, (length - width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3(length, 0, (length - width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3(length, 0, (length + width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length + width) / 2, 0, (length + width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
    } else if (side == RIGHT) {
        glm::vec3 vf = ldcorner + glm::vec3((length - width) / 2, 0, (length + width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length + width) / 2, 0, (length + width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length + width) / 2, 0, length);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length - width) / 2, 0, length);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
    } else if (side == DOWN) {
        glm::vec3 vf = ldcorner + glm::vec3(0, 0, (length - width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length - width) / 2, 0, (length - width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length - width) / 2, 0, (length + width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3(0, 0, (length + width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
    } else if (side == LEFT) {
        glm::vec3 vf = ldcorner + glm::vec3((length - width) / 2, 0, 0);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length + width) / 2, 0, 0);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length + width) / 2, 0, (length - width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length - width) / 2, 0, (length - width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
    } else if (side == CENTER) {
        glm::vec3 vf = ldcorner + glm::vec3((length - width) / 2, 0, (length - width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length + width) / 2, 0, (length - width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length + width) / 2, 0, (length + width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length - width) / 2, 0, (length + width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
    }
    indices.push_back(zero);
    indices.push_back(one);
    indices.push_back(two);
    indices.push_back(zero);
    indices.push_back(two);
    indices.push_back(three);
}

void Streets::CreateSimpleStreet(direction side, glm::vec3 ldcorner, float length, float width, float width2) {
    if (index + 4 > USHORT_MAX) {
        mesh.back()->InitFromData(vertices, normals, textureCoords, indices);
        mesh.push_back(new Mesh("streets"));
        index = 0;
        vertices = {};
        textureCoords = {};
        normals = {};
    }
    int zero = index;
    int one = zero + 1;
    int two = one + 1;
    int three = two + 1;
    index = index + 4;
    if (side == UP) {
        glm::vec3 vf = ldcorner + glm::vec3((length + width2) / 2, 0, (length - width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3(length, 0, (length - width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3(length, 0, (length + width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length + width2) / 2, 0, (length + width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
    } else if (side == RIGHT) {
        glm::vec3 vf = ldcorner + glm::vec3((length - width) / 2, 0, (length + width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length + width) / 2, 0, (length + width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length + width) / 2, 0, length);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length - width) / 2, 0, length);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
    } else if (side == DOWN) {
        glm::vec3 vf = ldcorner + glm::vec3(0, 0, (length - width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length - width2) / 2, 0, (length - width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length - width2) / 2, 0, (length + width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3(0, 0, (length + width) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
    } else if (side == LEFT) {
        glm::vec3 vf = ldcorner + glm::vec3((length - width) / 2, 0, 0);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length + width) / 2, 0, 0);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length + width) / 2, 0, (length - width2) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
        vf = ldcorner + glm::vec3((length - width) / 2, 0, (length - width2) / 2);
        vertices.push_back(vf);
        textureCoords.push_back(glm::vec2(-((float)CITY / 50) * vf.x + (float)CITY / 2, ((float)CITY / 50) * vf.z + (float)CITY / 2));
        normals.push_back(glm::vec3(0, 1, 0));
    }
    indices.push_back(zero);
    indices.push_back(one);
    indices.push_back(two);
    indices.push_back(zero);
    indices.push_back(two);
    indices.push_back(three);
}

void Streets::end(std::vector<glm::vec2> textureCoords, std::vector<glm::vec3> normals) {
    mesh.back()->InitFromData(vertices, this->normals, this->textureCoords, indices);
}