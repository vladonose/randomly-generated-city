#pragma once
#include <Core/GPU/Mesh.h>

#define USHORT_MAX 65535
#define SCITYS 5000
#define CITYS 100
#define CITY 100.0
#define CIRCLECORNERS 4

#define MB1H 15
#define MB1W 10

#define MB2H 8
#define MB2W 10

#define OB1H 8
#define OB1W 10

#define AP1H 17
#define AP1W 10

#define NOTHING -1
#define UNDEFINED 0
#define ROAD 1
#define HIGHWAY 2
#define LAND 3
#define WATER 4
#define PARK 5
#define BIGB 6

typedef struct {
    glm::vec3 lightPosition;
    glm::vec3 lightDirection;
    glm::vec3 lightColor;
    int spotlight;
    float angle;
} light_props;

typedef struct {
    float shininess;
    float kd;
    float ks;
} material_props;

typedef struct {
    int line, column;
} roadGenerator;

typedef struct {
    int up, down, left, right;
    int counter;
} roadCell;

typedef struct {
    int line, column;
    int size;
    bool interior;
} undiscovered;

typedef struct {
    bool up;
    bool down;
    bool left;
    bool right;
    int value;
} cell;

class Complex {
public:
    int size;
    cell **matrix;
    Mesh* mesh;
    glm::mat4 modelMatrix;

    Complex(int size);
    Complex();
    ~Complex();
};
