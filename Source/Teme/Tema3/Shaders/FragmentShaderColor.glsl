#version 330

#define no_lights 300

struct light_props {
	int Spotlight;
	float Angle;
	vec3 light_direction;
	vec3 light_position;
	vec3 light_color;
};

uniform light_props lights[no_lights];
uniform int max_lights;

// TODO: get color value from vertex shader
in vec3 world_position;
in vec3 world_normal;

// Uniforms for light properties
uniform vec3 eye_position;

uniform float material_kd;
uniform float material_ks;
uniform int material_shininess;

uniform vec3 object_color;
uniform int Spotlight;
uniform float Angle;
uniform vec3 light_direction;
uniform vec3 light_position;
uniform vec3 light_color;

layout(location = 0) out vec4 out_color;

vec3 getLight(vec3 colorToModify, vec3 V, float ambient_light, int i, float diffLightMult, float material_sh) {
	vec3 L = normalize(lights[i].light_position - world_position);
	vec3 H = normalize(L + V);
	float diffuse_light = material_kd * diffLightMult * max (dot(world_normal, L), 0);
	vec3 color;
	if (lights[i].Spotlight == 1) {
		float cut_off = radians(lights[i].Angle);
		float spot_light = dot(-L, lights[i].light_direction);
		float spot_light_limit = cos(cut_off);

		if (spot_light > cos(cut_off)) {
			// Quadratic attenuation
			float linear_att = (spot_light - spot_light_limit) / (1 - spot_light_limit);
			float atFactor = pow(linear_att, 2);
			float specular_light = 0.0;
			if (dot(world_normal, L) > 0) {
				specular_light = material_ks * pow(max(dot(world_normal, H), 0), material_sh);
			}
			float light = 2 * ambient_light + atFactor * (diffuse_light + specular_light);

			color = colorToModify * light;
		} else {
			float light = 2 * ambient_light;
			color = colorToModify * light;
		}
	} else {

		// TODO: compute specular light component
		float specular_light = 0.0;
		if (dot(world_normal, L) > 0) {
			specular_light = material_ks * pow(max(dot(world_normal, H), 0), material_sh);
		}

		float atFactor = 1 / (distance(lights[i].light_position, world_position));
	
		float light = ambient_light + atFactor * (diffuse_light + specular_light);

		color = colorToModify * light;
	}
	return color;
}

void main()
{
	vec3 V = normalize(eye_position - world_position);
	float ambient_light = 0.25;
	int i;
	vec3 colorToFin = getLight(object_color, V, 0.8, 0, 70, 120);
	vec3 colorS = vec3(object_color.x, object_color.y, object_color.z);
	for (i = 1; i < max_lights; i++) {
		vec3 color1 = getLight(colorS, V, 0.0000001, i, 0.000001, material_shininess);
		if (colorToFin != color1)
			colorToFin += color1;
	}
	out_color = vec4(colorToFin, 1);
}