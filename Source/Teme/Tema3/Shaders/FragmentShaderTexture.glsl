#version 330

#define no_lights 300

struct light_props {
	int Spotlight;
	float Angle;
	vec3 light_direction;
	vec3 light_position;
	vec3 light_color;
};

uniform light_props lights[no_lights];
uniform int max_lights;

in vec3 world_position;
in vec3 world_normal;
in vec2 texcoord;

uniform vec3 eye_position;

uniform float material_kd;
uniform float material_ks;
uniform int material_shininess;

uniform int Spotlight;
uniform float Angle;

uniform sampler2D texture;

layout(location = 0) out vec4 out_color;

vec3 getLight(vec3 colorToModify, vec3 V, float ambient_light, int i, float diffLightMult, float material_sh) {
	vec3 L = normalize(lights[i].light_position - world_position);
	vec3 H = normalize(L + V);
	float diffuse_light = material_kd * diffLightMult * max (dot(world_normal, L), 0);
	diffuse_light = 0;
	if (lights[i].Spotlight == 1) {
		float cut_off = radians(120);
		float spot_light = dot(-L, lights[i].light_direction);
		float spot_light_limit = cos(cut_off);
		if (spot_light > cos(cut_off)) {
			// Quadratic attenuation
			float linear_att = (spot_light - spot_light_limit) / (1 - spot_light_limit);
			float atFactor = pow(linear_att, 2);
			float specular_light = 0.0;
			if (dot(world_normal, L) > 0) {
				specular_light = material_ks * pow(max(dot(world_normal, H), 0), material_sh);
			}
			float light = 2 * ambient_light + atFactor * (diffuse_light + specular_light);

			return colorToModify * light;
		} else {
			float light = 2 * ambient_light;
			return colorToModify * light;
		}
	} else {

		// TODO: compute specular light component
		float specular_light = 0.0;
		if (dot(world_normal, L) > 0) {
			specular_light = material_ks * pow(max(dot(world_normal, H), 0), material_sh);
		}

		float atFactor = 1 / (distance(lights[i].light_position, world_position));
	
		float light = ambient_light + atFactor * (diffuse_light + specular_light);

		return colorToModify * light;
	}
}

void main()
{	
	vec3 V = normalize(eye_position - world_position);
	float ambient_light = 0.25;
	int i;
	vec4 colorToPass = texture2D(texture, texcoord);
	if (colorToPass.a < 0.5) {
		discard;
	}
	vec3 colorS = vec3(colorToPass.x, colorToPass.y, colorToPass.z);
	vec3 colorToFin = getLight(colorS, V, 1.1, 0, 70, 120);
	for (i = 1; i < max_lights; i++) {
		vec3 color1 = getLight(colorS, V, 0.00001, i, 0.0001, material_shininess);
		if (colorToFin != color1)
		colorToFin += color1;
	}
	out_color = vec4(colorToFin, 1);
}