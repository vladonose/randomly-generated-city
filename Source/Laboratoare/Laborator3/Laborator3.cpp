#include "Laborator3.h"

#include <vector>
#include <iostream>

#include <Core/Engine.h>
#include "Transform2D.h"
#include "Object2D.h"

using namespace std;

Laborator3::Laborator3()
{
}

Laborator3::~Laborator3()
{
}

void Laborator3::Init()
{
	glm::ivec2 resolution = window->GetResolution();
	auto camera = GetSceneCamera();
	camera->SetOrthographic(0, (float)resolution.x, 0, (float)resolution.y, 0.01f, 400);
	camera->SetPosition(glm::vec3(0, 0, 50));
	camera->SetRotation(glm::vec3(0, 0, 0));
	camera->Update();
	GetCameraInput()->SetActive(false);

	glm::vec3 corner = glm::vec3(0, 0, 0);
	float squareSide = 100;

	// compute coordinates of square center
	float cx = corner.x + squareSide / 2;
	float cy = corner.y + squareSide / 2;
	
	// initialize tx and ty (the translation steps)
	translateX = 150;
	translateY = 250;

	dxt = 5;
	dyt = 5;

	// initialize sx and sy (the scale factors)
	scaleX = 1;
	scaleY = 1;

	dxs = 0.01;
	dys = 0.01;
	counters = 0;
	
	// initialize angularStep
	angularStep = 0;
	dang = 0.02;
	
	angleBonusTr = 3.14f;
	bonusx = 0;
	bonusy = 0;
	bonusdx = 0;
	bonusdy = 0;
	dbangTr = 3.14f / 60;

	angleBonus = 0;
	dbang = 3.14f / 30;

	thingx = -1;
	thingy = -1;

	Mesh* square1 = Object2D::CreateSquare("square1", corner, squareSide, glm::vec3(1, 0, 0), true);
	AddMeshToList(square1);
	
	Mesh* square2 = Object2D::CreateSquare("square2", corner, squareSide, glm::vec3(0, 1, 0));
	AddMeshToList(square2);

	Mesh* square3 = Object2D::CreateSquare("square3", corner, squareSide, glm::vec3(0, 0, 1));
	AddMeshToList(square3);

	Mesh* square4 = Object2D::CreateSquare("square4", corner, squareSide, glm::vec3(0.4f, 0.4f, 0.4f));
	AddMeshToList(square4);
}

void Laborator3::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);
}

void Laborator3::Update(float deltaTimeSeconds)
{
	// TODO: update steps for translation, rotation, scale, in order to create animations

	glm::ivec2 resolution = window->GetResolution();
	modelMatrix = glm::mat3(1);
	translateX += dxt;
	translateY += dyt;
	if (translateX >= resolution.x - 100) {
		dxt = 0 - dxt;
	}
	else if (translateX <= 0) {
		dxt = 0 - dxt;
	}
	if (translateY >= resolution.y - 100) {
		dyt = 0 - dyt;
	}
	else if (translateY <= 0) {
		dyt = 0 - dyt;
	}
	modelMatrix *= Transform2D::Translate(translateX, translateY);
    modelMatrix *= Transform2D::Translate(50, 50);
    modelMatrix *= Transform2D::Rotate(angularStep);
    modelMatrix *= Transform2D::Translate(-50, -50);
	// TODO: create animations by multiplying current transform matrix with matrices from Transform 2D

	RenderMesh2D(meshes["square1"], shaders["VertexColor"], modelMatrix);

	modelMatrix = glm::mat3(1);
	scaleX += dxs;
	scaleY += dys;
	counters++;
	if (counters == 60) {
		counters = 0;
		dxs = 0 - dxs;
		dys = 0 - dys;
	}
	//modelMatrix *= Transform2D::Translate(450, 250);
    //modelMatrix *= Transform2D::Translate(-450, -250);
    modelMatrix *= Transform2D::Translate(450, 250);
	modelMatrix *= Transform2D::Scale(scaleX, scaleY);
	//TODO create animations by multiplying current transform matrix with matrices from Transform 2D
	
	RenderMesh2D(meshes["square2"], shaders["VertexColor"], modelMatrix);

	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(650, 250);
	angularStep += dang;
    //modelMatrix *= Transform2D::Translate(translateX, translateY);
	modelMatrix *= Transform2D::Translate(50, 50);
	modelMatrix *= Transform2D::Rotate(angularStep);
	modelMatrix *= Transform2D::Translate(-50, -50);

	//TODO create animations by multiplying current transform matrix with matrices from Transform 2D
	RenderMesh2D(meshes["square3"], shaders["VertexColor"], modelMatrix);
	bonusdx = (thingx * 150) * (cos(angleBonusTr) - cos(angleBonusTr - dbangTr));
	bonusdy = (thingy * 150) * (sin(angleBonusTr) - sin(angleBonusTr - dbangTr));
	angleBonusTr -= dbangTr;
	bonusx += bonusdx;
	bonusy += bonusdy;
	if (bonusx < 0) {
		bonusx = 0;
		thingx = -1;
		thingy = -1;
	}
	else if (bonusx >= resolution.x) {
		thingx = 1;
		//thingy = 1;
	}
	if (bonusy < 0) {
		bonusy = 0;
	}
	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(bonusx, bonusy);
	modelMatrix *= Transform2D::Translate(50, 50);
	angleBonus += dbang;
	modelMatrix *= Transform2D::Rotate(angleBonus);
	modelMatrix *= Transform2D::Translate(-50, -50);
	RenderMesh2D(meshes["square4"], shaders["VertexColor"], modelMatrix);
	if (angleBonusTr <= 0) {
		angleBonusTr = 3.14;
	}

}

void Laborator3::FrameEnd()
{

}

void Laborator3::OnInputUpdate(float deltaTime, int mods)
{
	
}

void Laborator3::OnKeyPress(int key, int mods)
{
	// add key press event
}

void Laborator3::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Laborator3::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event
}

void Laborator3::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
}

void Laborator3::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Laborator3::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Laborator3::OnWindowResize(int width, int height)
{
}
