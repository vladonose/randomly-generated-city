#pragma once

#include <include/glm.h>
#include <math.h>

namespace Transform2D
{
	// Translate matrix
	inline glm::mat3 Translate(float translateX, float translateY)
	{
		// TODO implement translate matrix
		return glm::mat3(1, 0, 0,
						 0, 1, 0,
						 translateX, translateY, 1);
	}

	// Scale matrix
	inline glm::mat3 Scale(float scaleX, float scaleY)
	{
		// TODO implement scale matrix
		return glm::mat3(scaleX, 0, 0,
						 0, scaleY, 0,
						 0, 0, 1);
	}

	// Rotate matrix
	inline glm::mat3 Rotate(float radians)
	{
		double cr = cos(radians),
			sr = sin(radians),
			msr = 0 - sin(radians);
		// TODO implement rotate matrix
		return glm::mat3(cr, sr, 0,
						 msr, cr, 0,
						 0, 0, 1);
	}
}
