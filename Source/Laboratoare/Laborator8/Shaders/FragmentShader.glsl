#version 330

// TODO: get color value from vertex shader
in vec3 world_position;
in vec3 world_normal;

// Uniforms for light properties
uniform vec3 light_direction;
uniform vec3 light_position;
uniform vec3 eye_position;

uniform float material_kd;
uniform float material_ks;
uniform int material_shininess;

uniform vec3 object_color;
uniform int Spotlight;
uniform float Angle;

layout(location = 0) out vec4 out_color;

void main()
{
	vec3 L = normalize(light_position - world_position);
	vec3 V = normalize(eye_position - world_position);
	vec3 H = normalize(L + V);
	float ambient_light = 0.25;
	float diffuse_light = material_kd * max (dot(world_normal, L), 0);
	if (Spotlight == 1) {
		float cut_off = radians(Angle);
		float spot_light = dot(-L, light_direction);
		float spot_light_limit = cos(cut_off);

		if (spot_light > cos(cut_off)) {
			// Quadratic attenuation
			float linear_att = (spot_light - spot_light_limit) / (1 - spot_light_limit);
			float atFactor = pow(linear_att, 2);
			float specular_light = 0.0;
			if (dot(world_normal, L) > 0) {
				specular_light = material_ks * pow(max(dot(world_normal, H), 0), material_shininess);
			}
			float light = 2 * ambient_light + atFactor * (2 * diffuse_light + specular_light);

			out_color = vec4(object_color * light, 1);
		} else {
			float light = 2 * ambient_light;
			out_color = vec4(object_color * light, 1);
		}
	} else {

		// TODO: compute specular light component
		float specular_light = 0.0;
		if (dot(world_normal, L) > 0) {
			specular_light = material_ks * pow(max(dot(world_normal, H), 0), material_shininess);
		}

		float atFactor = 1 / (distance(light_position, world_position));
	
		float light = 2 * ambient_light + atFactor * (2 * diffuse_light + specular_light);

		out_color = vec4(object_color * light, 1);
	}
	//out_color = vec4(object_color, 1);
}