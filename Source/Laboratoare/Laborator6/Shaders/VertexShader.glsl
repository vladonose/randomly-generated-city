#version 330

// TODO: get vertex attributes from each location

// Uniform properties
uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

uniform float e_time;

// TODO: output values to fragment shader
layout(location = 0) in vec3 v_position;
layout(location = 3) in vec3 v_normal;
layout(location = 1) in vec3 v_color;

out vec3 frag_color;

void main()
{
	// TODO: send output to fragment shader
	frag_color = v_color;

	// TODO: compute gl_Position
	gl_Position = Projection * View * Model * vec4(v_position, 1.0);
}
