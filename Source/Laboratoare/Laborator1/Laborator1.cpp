#include "Laborator1.h"

#include <vector>
#include <iostream>

#include <Core/Engine.h>

using namespace std;
float blue = 0;
double scale = 0.01;
int gpresses = 0;
float x = 0;
float y = 0;
float z = 0;
// Order of function calling can be seen in "Source/Core/World.cpp::LoopUpdate()"
// https://github.com/UPB-Graphics/Framework-EGC/blob/master/Source/Core/World.cpp

Laborator1::Laborator1()
{
}

Laborator1::~Laborator1()
{
}

void Laborator1::Init()
{
	// Load a mesh from file into GPU memory
	{
		Mesh* mesh = new Mesh("box");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "box.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}
	{
		Mesh* anothermesh = new Mesh("Archer");
		anothermesh->LoadMesh(RESOURCE_PATH::MODELS + "Characters/Archer", "Archer.fbx");
		meshes[anothermesh->GetMeshID()] = anothermesh;
	}
}

void Laborator1::FrameStart()
{

}

void Laborator1::Update(float deltaTimeSeconds)
{
	glm::ivec2 resolution = window->props.resolution;

	// sets the clear color for the color buffer
	glClearColor(0, 0, blue, 1);

	// clears the color buffer (using the previously set color) and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);

	// render the object
	RenderMesh(meshes["box"], glm::vec3(1, 0.5f, 0), glm::vec3(0.5f));
	RenderMesh(meshes["Archer"], glm::vec3(x, y, z), glm::vec3(scale));

	// render the object again but with different properties
	RenderMesh(meshes["box"], glm::vec3(-1, 0.5f, 0));

}

void Laborator1::FrameEnd()
{
	DrawCoordinatSystem();
}

// Read the documentation of the following functions in: "Source/Core/Window/InputController.h" or
// https://github.com/UPB-Graphics/Framework-EGC/blob/master/Source/Core/Window/InputController.h

void Laborator1::OnInputUpdate(float deltaTime, int mods)
{
	// treat continuous update based on input
};

void Laborator1::OnKeyPress(int key, int mods)
{
	// add key press event
	if (key == GLFW_KEY_F) {
		blue = 0.5;
	}
	else if (key == GLFW_KEY_G) {
		if (gpresses == 0)
		{
			Mesh* mesh = meshes["Archer"];
			mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "box.obj");
			meshes[mesh->GetMeshID()] = mesh;
			scale = 0.5f;
			gpresses = 1;
		}
		else if (gpresses == 1) {
			Mesh* mesh = meshes["Archer"];
			mesh->LoadMesh(RESOURCE_PATH::MODELS + "Characters/Archer", "Archer.fbx");
			meshes[mesh->GetMeshID()] = mesh;
			scale = 0.01f;
			gpresses = 2;
		}
		else {
			Mesh* mesh = meshes["Archer"];
			mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "teapot.obj");
			meshes[mesh->GetMeshID()] = mesh;
			scale = 1.0f;
			gpresses = 0;
		}
	}
	else if (key == GLFW_KEY_UP) {
		z++;
	}
	else if (key == GLFW_KEY_DOWN) {
		z--;
	}
	else if (key == GLFW_KEY_LEFT) {
		x++;
	}
	else if (key == GLFW_KEY_RIGHT) {
		x--;
	}
	else if (key == GLFW_KEY_1) {
		y++;
	}
	else if (key == GLFW_KEY_2) {
		y--;
	}
};

void Laborator1::OnKeyRelease(int key, int mods)
{
	// add key release event
};

void Laborator1::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event
};

void Laborator1::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
};

void Laborator1::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Laborator1::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
	// treat mouse scroll event
}

void Laborator1::OnWindowResize(int width, int height)
{
	// treat window resize event
}
